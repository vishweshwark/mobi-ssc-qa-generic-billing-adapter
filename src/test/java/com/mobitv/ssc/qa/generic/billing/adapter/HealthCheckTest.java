package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;
import com.sun.jersey.api.client.ClientResponse;

/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter - Health Check
*
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6696
*/
public class HealthCheckTest extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("WithoutPurchaseObject_Type1.properties");

	
	 /* @BeforeClass public static void initialConditions(){
	  
	  genericUtil.uploadToFTP(
	  "./src/test/resources/config/WithoutPurchaseObject_Type1/partner-config");
	  
	  try { Thread.sleep(60000); } catch (InterruptedException e1) { e1.printStackTrace(); } 
	 }*/
	 
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();
			account.put("status", "");
			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}

	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  If Partner Config contains valid entries, verify Health Check response returns status = UP, config file = true, and all other downstream components = true
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544904
	 */
	@Test
	public void SSC_GenericBillingAdapter_HealthCheck_001() {
		try {
			log.info("starting test case 1");
			
			log.info("uploading partner config");
			genericUtil.uploadToFTP("./src/test/resources/config/HealthCheck1/partner-config");
			
			Thread.sleep(60000);
			log.info("1 min sleep");
			
			log.info("Health check API Call, Partner config with Leading space");
			ClientResponse clientResponse = genericUtil.healthCheckResponse();

			log.info("Verifying the Response");
			log.info("response Status code : " + clientResponse.getStatus() + " Response Message :" + clientResponse.getEntity(String.class).toString());
			assertEquals(200, clientResponse.getStatus());

			log.info("Ending test case 1");
			
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}

	}

	// No Entries In partner Config.
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  If Partner Config file contains no entries, verify Health Check response returns status = UP, config file = true, and all other downstream components = true

	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544905
	 */
	@Test
	public void SSC_GenericBillingAdapter_HealthCheck_002() {
		try {
			log.info("starting test case 2");
			
			log.info("uploading partner config");
			genericUtil.uploadToFTP("./src/test/resources/config/HealthCheck2/partner-config");
			
			Thread.sleep(60000);
			log.info("1 min sleep");
			
			log.info("Health check API Call, Partner config with No Entries");
			ClientResponse clientResponse = genericUtil.healthCheckResponse();

			log.info("Verifying the Response");
			log.info("response Status code : " + clientResponse.getStatus() + " Response Message :" + clientResponse.getEntity(String.class).toString());
			assertEquals(200, clientResponse.getStatus());

			log.info("Ending test case 2");
			
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	// Partner config with Leading space
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description   If Partner Config file contains entries with leading spaces, verify Health Check response returns status = UP, config file = true, and all other downstream components = true

	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544906
	 */
	@Test
	public void SSC_GenericBillingAdapter_HealthCheck_003() {
		try {
			log.info("starting test case 3");
			
			log.info("uploading partner config");
			genericUtil.uploadToFTP("./src/test/resources/config/HealthCheck3/partner-config");
			
			Thread.sleep(60000);
			log.info("1 min sleep");
			
			log.info("Health check API Call, Partner config with Leading space");
			ClientResponse clientResponse = genericUtil.healthCheckResponse();

			log.info("Verifying the Response");
			log.info("response Status code : " + clientResponse.getStatus() + " Response Message :" + clientResponse.getEntity(String.class).toString());
			assertEquals(200, clientResponse.getStatus());

			log.info("Ending test case 3");
			
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	// Partner config with trailing space
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  If Partner Config file contains entries with trailing spaces, verify Health Check response returns status = UP, config file = true, and all other downstream components = true

	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2108428
	 */
	@Test
	public void SSC_GenericBillingAdapter_HealthCheck_004() {
		try {
			log.info("starting test case 4");
			
			log.info("uploading partner config");
			genericUtil.uploadToFTP("./src/test/resources/config/HealthCheck4/partner-config");
			
			Thread.sleep(60000);
			log.info("1 min sleep");
			
			log.info("Health check API Call, Partner config with Trailing space");
			ClientResponse clientResponse = genericUtil.healthCheckResponse();

			log.info("Verifying the Response");
			log.info("response Status code : " + clientResponse.getStatus() + " Response Message :" + clientResponse.getEntity(String.class).toString());
			assertEquals(200, clientResponse.getStatus());

			log.info("Ending test case 4");
			
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}

	}
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  If Partner Config file contains malformed entries, verify Health Check response returns status = UP, config file = true, and all other downstream components = true.

	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2108429
	 */
	@Test
	public void SSC_GenericBillingAdapter_HealthCheck_005() {
		try {
			log.info("starting test case 5");

			log.info("uploading partner config");
			genericUtil.uploadToFTP("./src/test/resources/config/HealthCheck5/partner-config");

			Thread.sleep(60000);
			log.info("1 min sleep");

			log.info("Health check API Call, Partner config with Malformed Entries");
			ClientResponse clientResponse = genericUtil.healthCheckResponse();

			log.info("Verifying the Response");
			log.info("response Status code : " + clientResponse.getStatus() + " Response Message :" + clientResponse.getEntity(String.class).toString());
			assertEquals(200, clientResponse.getStatus());

			log.info("Ending test case 5");

		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}

	}
}
