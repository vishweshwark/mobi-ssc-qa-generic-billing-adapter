package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;

public class RegisterProfileAPITest extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");
	

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("SupportActiveStatus.properties");


	  @BeforeClass
	  public static void initialConditions(){
	  
	 genericUtil.uploadToFTP(
	 "./src/test/resources/config/SupportActiveAccount/partner-config");
	 
	  
	 try { Thread.sleep(60000); } catch (InterruptedException e1) { 
	  e1.printStackTrace(); } 
	 }
	 

	private void generatePurchaseRequest() {

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();

			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  For a new provisioning account, register profile call for creating purchase with various partner accounts should return 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2661053
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_001() {
		try {
			log.info("Starting test case 1");
			
			/*genericUtil.uploadToFTP(
					 "./src/test/resources/config/RegisterProfileAPI/01/partner-config");
					 
					  
			Thread.sleep(60000); */
			
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", "1.0");

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			log.info("Ending test case 1");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description Verify register profile call for creating account with various partner accounts with only fips_code(partner) returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506742
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_002() {
		try {
			log.info("Starting test case 2");
			
			genericUtil.uploadToFTP(
					 "./src/test/resources/config/RegisterProfileAPI/02/partner-config");
					 
					  
			Thread.sleep(60000); 
			
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call with Fips Code");
			account.put("status", "active");
			account.put("fips_code", "49013");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			log.info("Ending test case 2");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 2");
			fail();
		}
	}
	
	//test cases automated is wrong have to verify it will amit N
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description Verify register profile call for creating account with various partner accounts with only equipment_id(partner) returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506743
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_003() {
		try {
			log.info("Starting test case 3");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/01/partner-config");
			 
			  
	Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call with Fips Code");
			account.put("status", "null");
			
			log.info("create purchase request with equipment_id");
			
			addEquipmentId("equipment_id", "ssc-9934-test", extendedproperty);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
		
			log.info("Ending test case 3");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 3");
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  Verify register profile call with various partner accounts with only fips_code(partner) & region(default) returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506744
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_004() {
		try {
			log.info("Starting test case 3");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/04/partner-config");
			 
			  
	Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call with Fips Code");
			account.put("status", "active");
			
			account.put("fips_code", "49013");
			account.put("region", "meridian");
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			log.info("Ending test case 4");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 4");
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  Verify register profile call for creating account with various partner accounts with special characters(like '-,_,@,!, etc) in account returns 200 .
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506745
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_005() {
		try {
			log.info("Starting test case 5");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/05/partner-config");
			 
			  
	Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call with Fips Code");
			account.put("status", "active");
			
			account.put("fips_code", "49013");
			account.put("region", "meridi@n");
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			log.info("Ending test case 5");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 5");
			fail();
		}
	}
	//failing test case have to check it with amit need to chanege partner config
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description If partner sends notification to create account without purchases and having fips_code & region as default, verify response should return 400.
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506746
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_006() {
		try {
			log.info("Starting test case 6");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/01/partner-config");
			 
			  
			Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call with Fips Code");
			account.put("status", "null");
			addEquipmentId("equipment_id","ssc-9934-abc-test", extendedproperty);
			reqobj.remove("purchase");
			
			log.info("create purchase request with equipment_id");
			
			addEquipmentId("equipment_id", "ssc-9934-test", extendedproperty);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
		
			log.info("Ending test case 6");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 6");
			fail();
		}
	}
	
	public void addEquipmentId(String name,String value,JSONObject objectname){
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			account.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
}
