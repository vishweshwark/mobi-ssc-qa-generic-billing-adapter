package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;
/**
*
* @author 
*
* @test.suite.description
* 	
Generic Billing Adapter_Reactivation of suspended account with same purchases_Sprint 73
*
* @test.prerequisite
* Reactivation of suspended account with same purchases	

*
*
* @test.reference
*https://testrail.mobitv.corp/index.php?/runs/view/7035
*/
public class ReActAcctSamePackage extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;
	
	JSONArray emptyPurchaseArray = new JSONArray();
	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("ReActAcctSpicificPackage.properties");

	
	@BeforeClass 
	  public static void initialConditions(){
	 
	  genericUtil.uploadToFTP("./src/test/resources/config/ReActAcctSpecificPackage/partner-config");
	  
	  
	  try {
		  Thread.sleep(60000); 
	  }
	  catch (InterruptedException e1) 
	  { // TODO
	 e1.printStackTrace();
	 }
	  }
	 
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	private void generatePurchaseRequest() {
		
		
		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();
			
			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspend account and cancelled purchases, if user sends request for reactivating purchases with account status as null, verify response returns 200 and account status will be activated in the account table.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6201017
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_001(){
		try {
			log.info("");
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			account.remove("status");
			account.put("status", "active");
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "MAX");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			reqobj.put("purchase", purchasearray);
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(2, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the disconnected account, if user sends request for reactivation of purchases with account status as null, verify response must throw error message as you can't re-active the account once it is disconnected.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6201018
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_002(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			account.remove("status");
			account.put("status", "active");
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "MAX");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			reqobj.put("purchase", purchasearray);
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspended account and active purchases, if user sends request for cancelling few packages with account status as null, verify response should return 200 and purchases should be cancelled.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6201019
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_003(){
		try {
			generatePurchaseRequest();
			
			JSONObject purchase1 = getPurchase();
			JSONObject purchase2 = getPurchase();
			JSONObject purchase3 = getPurchase();
			JSONObject purchase4 = getPurchase();
			
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					//addQuantity("name", "2", purchase);
					
					purchase1.put("product_id", "TV2EXP");
					//addQuantity("name", "4", purchase);
					
					purchase2.put("product_id", "TV2JXN");
					//addQuantity("name", "6", purchase);
					
					purchasearray.put(purchase1);
					purchasearray.put(purchase2);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				purchase2.put("product_id", "HBO");
				
				purchasearray.put(purchase1);
				purchasearray.put(purchase2);
			}
			
			
			
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			
			//create
			generatePurchaseRequest();
			String status =null;
			account.remove("status");
			account.put("status",status);
			
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			purchase1.remove("action");
			purchase1.put("action", "cancel");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					
					purchase.put("product_id", "TV2COL");
					
					
					purchase1.put("product_id", "TV2EXP");
		
					
			
					
					purchasearray.put(purchase1);
					

			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				
				purchasearray.put(purchase1);
				
			}
			
		
			
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History" + count);
			//assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  With the suspended account and active purchases, if user sends request for new purchases of few packages with account status as null, verify response should return 200 and purchases should be created.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2487715
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_004(){
		try {
			generatePurchaseRequest();
			
			JSONObject purchase1 = getPurchase();
			JSONObject purchase2 = getPurchase();
			JSONObject purchase3 = getPurchase();
			JSONObject purchase4 = getPurchase();
			
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					//addQuantity("name", "2", purchase);
					
					purchase1.put("product_id", "TV2EXP");
					//addQuantity("name", "4", purchase);
					
					purchase2.put("product_id", "TV2JXN");
					//addQuantity("name", "6", purchase);
					
					purchasearray.put(purchase1);
					purchasearray.put(purchase2);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				purchase2.put("product_id", "HBO");
				
				purchasearray.put(purchase1);
				purchasearray.put(purchase2);
			}
			
			
			
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			
			//create
			generatePurchaseRequest();
			String status =null;
			account.remove("status");
			account.put("status",status);
			
			purchase.remove("action");
			purchase.put("action", "create");
			
			purchase1.remove("action");
			purchase1.put("action", "create");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					
					purchase.put("product_id", "TV2LIFE");
					
					
					purchase1.put("product_id", "TV2MER");
		
					
			
					
					purchasearray.put(purchase1);
					

			}else{
				purchase.put("product_id", "Express");
				purchase1.put("product_id", "Essentials");
				
				purchasearray.put(purchase1);
				
			}
			
		
			
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History" + count);
			//assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspend account and cancelled purchases, if user sends request for reactivating purchases with account status as empty, verify response returns 200 and account status will be activated in the account table.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6201021
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_005(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
		
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			String status = null;
			account.remove("status");
			account.put("status", status);
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "TV2LIFE");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			purchase.remove("action");
			purchase.put("action", "create");
			reqobj.put("purchase", purchasearray);
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspend acct & cancelled purchases, if user sends request for reactivating purchases with acct status as null & re-activating back the same purchase with acct status as null, verify response returns 200 and acct status will be active
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6201022
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_006(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "HBO");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
		
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			String status = null;
			account.remove("status");
			account.put("status", status);
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "TV2COL");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			purchase.remove("action");
			purchase.put("action", "create");
			reqobj.put("purchase", purchasearray);
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspend account and active purchases, if user sends request for reactivating account with only account object and account status as null, verify response returns 200 and account should be re-activated in the account table.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6292754
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_007(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "HBO");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			String status = null;
			account.remove("status");
			account.put("status", status);
			
			reqobj.remove("purchase");
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(1, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspend account and active purchases, if user sends request for reactivating account with only account object and account status as ""(empty), verify response returns 200 and account should be re-activated in the account table.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6292767
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_008(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "HBO");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			String status = "";
			account.remove("status");
			account.put("status", status);
			
			reqobj.remove("purchase");
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(1, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  With the suspend acct and active purchases, if user sends request for reactivating acct with empty purchase object along with acct object and acct status as null, verify response returns 200 and acct status should be re-activated in the acct table
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2496485
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_009(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "HBO");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			
			String status = null;
			account.remove("status");
			account.put("status", status);
			/*purchase.remove("action");
			purchase.remove("product_id");
			purchase.remove("vendor_purchase_id");
			purchase.remove("start_date");
			purchase.remove("purchase_origin");
			purchase.remove("is_trial");*/
			reqobj.put("purchase", emptyPurchaseArray);
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(1, count);
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With suspend acct and active purchases, if user sends request for reactivating acct with empty purchase object along with acct object and acct status as "" (empty), verify response returns 200 and acct status should be re-activated in acct table
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2496486
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Reactivation_suspended_account_same_purchases_010(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "HBO");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			
			String status = "";
			account.remove("status");
			account.put("status", status);
			/*purchase.remove("action");
			purchase.remove("product_id");
			purchase.remove("vendor_purchase_id");
			purchase.remove("start_date");
			purchase.remove("purchase_origin");
			purchase.remove("is_trial");*/
			reqobj.put("purchase", emptyPurchaseArray);
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(1, count);
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
}
