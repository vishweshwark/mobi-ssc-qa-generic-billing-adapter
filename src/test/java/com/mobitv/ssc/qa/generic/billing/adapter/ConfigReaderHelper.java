package com.mobitv.ssc.qa.generic.billing.adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




public class ConfigReaderHelper {

	private static final Logger logger = LoggerFactory.getLogger(ConfigReaderHelper.class);
	
	
	//private String configDirectoryLocation="/mobi-ssc-qa-generic-billing-adapter/src/test/resources/config";
	private String configDirectoryLocation="./src/test/resources/com/mobitv/ssc/qa/generic/billing/adapter/config";
	
	public ConfigReaderHelper() {
		loadapplicationConfigPropertiesFromFile();
	}
	
	private String configFileName="partner-config.properties";
	
	public ConfigReaderHelper(String string) {
		// TODO Auto-generated constructor stub
		configFileName  = string;
		loadapplicationConfigPropertiesFromFile();
	}
	private Properties configProperties;
	
	public void reloadPartnerConfig() {
		logger.info("load properties file invoked at {}",Calendar.getInstance().getTimeInMillis());
		//TODO REMOVE THE USE OF THE CONFIG
		loadapplicationConfigPropertiesFromFile();
		logger.info("loading of properties file completed at {}",Calendar.getInstance().getTimeInMillis());
	}

	public String getConfigValue(String key) {
		return configProperties.getProperty(key);
	}
	
	public void setConfigFile(String string){
		configFileName = string;
	}
	public Long getConfigLongValue(String key){
		String timeStampValidity= configProperties.getProperty(key);
		return Long.parseLong(timeStampValidity);
	}
	
	public void loadapplicationConfigPropertiesFromFile() {

		String filePath = getPartnerConfigFilePath();
		BufferedReader br = null;
		try {
			String fullyQualifiedConfigFilePath = configDirectoryLocation+"/"+configFileName;
			logger.info("Config file location path : {}",fullyQualifiedConfigFilePath);
			configProperties = new Properties();
			configProperties.load(new FileInputStream(new File(configDirectoryLocation+"/"+configFileName)));
		} catch (IOException e) {
			logger.error("Exception occurred in loadapplicationConfigPropertiesFromFile() method: " + e);
		} finally {

			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				logger.error("Exception occurred in loadapplicationConfigPropertiesFromFile() method: " + ex);
			}
		}
	}

	public String getPartnerConfigFilePath() {
		String path = configDirectoryLocation +"/"+ configFileName;
		logger.info("config file path :" + path);
		return path;
	}

}
