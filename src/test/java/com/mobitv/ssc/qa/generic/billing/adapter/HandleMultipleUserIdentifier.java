package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv
	/**
	*
	* @author 
	*
	* @test.suite.description
	* 	SSC_Sprint68_GenericBillingAdapter_HandleMultiUserIDs_SSC-7777
	*
	* @test.prerequisite
	* Handle Multiple User identifiers
	*
	*
	* @test.reference
	* https://testrail.mobitv.corp/index.php?/runs/view/6730
	*/
//TODO changes according to the aprtner config change.
@Ignore
public class HandleMultipleUserIdentifier extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("MultipleUserIdentifier_Type1.properties");
	
	@BeforeClass
	public static void initialConditions(){
		System.out.println("abcd");
		genericUtil.uploadToFTP("./src/test/resources/config/MultipleUserIdentifier_Type1/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
	}
	
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If partner config file has multiple user IDs configured and partner sends purchase notification w/ ID, verify purchase is processed, response returns 200, and in AM DB, partner_account table has both DefaultUserIdentifer & SecondaryUserIdentifier  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585589
	 */
	
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_001(){
		
		log.info("Starting test case 01");
		generatePurchaseRequest();
		
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase ");
			
			
			purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			/*account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");*/
			account.put("zip_code", "84034");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertNotNull(directlink_user_id);
			assertNotNull(fips_code);
			assertNotNull(region);
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 01....");
		}
		
		log.info("ending test case 01....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *If partner config file has multiple user IDs configured and partner sends purchase notification w/out ID, verify purchase is processed, response returns 200, and in AM DB, partner_account table contains DefaultUserIdentifer 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585590
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_002(){
		
		log.info("Starting test case 2nd");
		generatePurchaseRequest();
		
		
		try {
			log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase without notification identifier ");
			
			
			//purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			/*account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");*/
			account.put("zip_code", "84034");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertNotNull(directlink_user_id);
			assertNotNull(fips_code);
			assertNotNull(region);
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 2nd....");
		}
		
		log.info("ending test case 2nd....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 	If partner config file has multi. user IDs configured, partner sends purchase notif. w/ ID, a cancel notif. is sent w/ same ID, verify cancel is processed, response returns 200, partner_account table has DefaultUserIdentifer & SecondaryUserIdentifier 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585592
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_003(){
		
		log.info("Starting test case 3rd");
		generatePurchaseRequest();
		
		
		try {
			log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase with notification identifier ");
			
			
			purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			/*account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");*/
			account.put("zip_code", "84036");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "cancel");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 3rd....");
		}
		
		log.info("ending test case 3rd....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If partner config file has multi. user IDs configured, partner sends purchase notif. w/out ID, a cancel notif. w/out ID, verify cancel is processed, response returns 200, partner_account table has DefaultUserIdentifer 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585593
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_004(){
		
		log.info("Starting test case 4th");
		generatePurchaseRequest();
		
		
		try {
			log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase with notification identifier ");
			
			
			//purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			/*account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");*/
			account.put("zip_code", "84036");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				//purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "cancel");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 4th....");
		}
		
		log.info("ending test case 4th....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner config file has multi. user IDs config'd, partner sends purch. notif. w/out ID, cancel notif. is sent w/ ID, verify cancel is processed, resp. ret. 200, and in AM DB, partner_account table has DefaultUserIdentifer & SecondaryUserIdentifier 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585594
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_005(){
		
		log.info("Starting test case 5th");
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		try {
		
		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase with notification identifier ");
			
			
			//purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");
			account.put("zip_code", "84036");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "cancel");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 5th....");
		}
		
		log.info("ending test case 5th....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner config file has multi. user IDs config'd, partner sends purch. notif. w/ ID1, cancel notif. is sent w/ ID2, verify cancel is processed, resp. ret. 200, and in AM DB, partner_account table has DefaultUserIdentifer & ID2 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585595
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_006(){
		
		log.info("Starting test case 6th");
		generatePurchaseRequest();
		
		
		try {
			log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase with notification identifier ");
			
			
			purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");
			account.put("zip_code", "84036");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "cancel");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
		
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 6th....");
		}
		
		log.info("ending test case 6th....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner config file has multi. user IDs configured, partner sends purch. notif. w/ ID1, cancel notif. is sent w/ ID2, re-purchase w/ ID3, response returns 200, and in AM DB, partner_account table has DefaultUserIdentifer & ID3 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 directlink.fips.code.config=external_system_zip
	 directlink.fips.code.default=52525
	 directlink.region.config=default

	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5585599
	 */
	@Test
	public void genericBillingAdapter_SSC_HandleMultUserIDs_007(){
		
		log.info("Starting test case 7th");
		generatePurchaseRequest();
		
		
		try {
			log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".user.identifiers").equals(carrier+"_user_id:external_id,equipment_id:account_extended_property")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase with notification identifier ");
			
			
			purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
			//addQuantity("extended-property-name_dpoot_SSC-7777b", "extended-property-value_dpoot_SSC-7777b", purchase);
			
			account.put("county","jackson");
			account.put("state","state");	
			account.put("region","meridian");
			account.put("zip_code", "84036");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchase Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			String directlink_user_id = null;
			String fips_code= null;
			String region = null;
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String partneraccountid = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				
				if ("ezv_user_id".equalsIgnoreCase(partnername)) {
					directlink_user_id = partnername;
				}
				else if ("fips_code".equalsIgnoreCase(partnername)) {
					fips_code = partnername;
				}
				else if ("region".equalsIgnoreCase(partnername)) {
					region = partnername;
				}
				
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "cancel");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
			
			{
				purchase.put("notification_identifier", "paytv:ssc:create:dpoot_SSC-7712"+external_id);
				log.info("Calling to create an Purchase with notification identifier ");
				purchase.put("action", "create");
				purchase.remove("start_date");
				purchase.put("expires_date", xml_start_date);
				purchase.put("cancel_date",xml_start_date);
				purchase.put("reason_code", "0000");
				purchase.remove("notification_identifier");
				jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				i = 0;
				 subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				directlink_user_id = null;
				fips_code= null;
				region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("ezv_user_id".equalsIgnoreCase(partnername)) {
						directlink_user_id = partnername;
					}
					else if ("fips_code".equalsIgnoreCase(partnername)) {
						fips_code = partnername;
					}
					else if ("region".equalsIgnoreCase(partnername)) {
						region = partnername;
					}
					
				}
				log.info("Verifying the Creation of of purchase in DB");
				assertNotNull(directlink_user_id);
				assertNotNull(fips_code);
				assertNotNull(region);
				assertEquals("exist", subscriptionUserStatus);
								
			}
			
		}else{
			log.info("Carrier.user.identifier is not equal to directlink_user_id:external_id,equipment_id:account_extended_property");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 7th....");
		}
		
		log.info("ending test case 7th....");
		
	}
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

}
}
