package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.mobitv.MobiTestBase;
import com.mobitv.commons.restclient.webclient.RestRequest;
import com.mobitv.commons.restclient.webclient.RestRequest.HttpMethodType;
import com.mobitv.convergence.common.ServerFinder;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.util.JDBCHelper;
import com.sun.jersey.api.client.ClientResponse;

public class GenericUtil extends MobiTestBase {

	private  String carrier = config.getProperty("Carrier");
	private  String product =config.getProperty("Product");
	private String partner=config.getProperty("Partner");
	
	String serviceDiscoveryHost;
	String backEndComponent=config.getProperty("billing.back.end.component");
	String backEndHost=config.getProperty("billing.back.end.Host");
	String backEndPort=config.getProperty("billing.back.end.Port");
	
	private String host ="svcar01p1.func-cp.qa.smf1.mobitv";
    private String port ="8081";
    private Random rand = new Random();
    private static final String DIRECTLINKSECRET = "ZGlyZWN0bGlua19hcHA6cGF5dHZAI3RzZ3IhMDk4OjE0OTQ2MzI1OTA6aW50";// Random secret key
    private static final String CSPIRESECRET = "0f0d4f0e3d501af466f1d59831a7bbc440292c6d";
    private String Secret = config.getProperty(carrier+"secret");
    
    //Server Credentials
    private String uploadPath  = "/opt/mobi-ssc-generic-billing-adapter/config";
    private String userName = config.getProperty("server.username");
    private String password = config.getProperty("server.password");
    private String server = config.getProperty("QA_PLAT_SVCAR");
    private String server2 = config.getProperty("QA_PLAT_SVCAR_SERVER_TWO");
    // PM
 	private final String purchDb = "purchase_managers";
 	// AM
 	private final String acctDb = "ACCTMGMT";
 	// RM
 	private final String rightsDb = "rights_manager";
	
	// Database configs
	// PM
    private String purchMgrDb = config.getProperty("QA_PM_DB");
    private String purchMgrDriver = config.getProperty("jdbc.connector" + purchMgrDb + "Driver");
    private String purchMgrUrl = config.getProperty("jdbc.purhmgr" + purchMgrDb + "Url");
    private String purchMgrUser = config.getProperty("Purhmgr" + purchMgrDb + "User");
    private String purchMgrPwd = config.getProperty("Purhmgr" + purchMgrDb + "Pwd");

    //AM
	private String acctMgrDb = config.getProperty("QA_AM_DB");
	private String acctMgrDriver = config.getProperty("jdbc.connector"+acctMgrDb+"Driver");
	private String acctMgrUrl = config.getProperty("jdbc.acctmgr"+acctMgrDb+"Url");
	private String acctMgrUser = config.getProperty("Acctmgr"+acctMgrDb+"User");
	private String acctMgrPwd = config.getProperty("Acctmgr"+acctMgrDb+"Pwd");  
    
    //RM
    private String rightsMgrDb = config.getProperty("QA_RM_DB");
    private String rightsMgrDriver = config.getProperty("jdbc.connector" + rightsMgrDb + "Driver");
    private String rightsMgrUrl = config.getProperty("jdbc.rightsmgr" + rightsMgrDb + "Url");
    private String rightsMgrUsr = config.getProperty("Rightsmgr" + rightsMgrDb + "User");
    private String rightsMgrPwd = config.getProperty("Rightsmgr" + rightsMgrDb + "Pwd");	

    
    public GenericUtil()
    {
    	try {
			URI discoveredHost = ServerFinder.getServer(backEndComponent, config);
			serviceDiscoveryHost = discoveredHost.toASCIIString();
			log.info("Generic Billing Service Discovered Host: " + serviceDiscoveryHost);
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Exception occoured while service discovery", ex);
			serviceDiscoveryHost = null;
		}
    }
    private String formBaseUrl() {
	
			//if (serviceDiscoveryHost != null && !(serviceDiscoveryHost.isEmpty()))
    	if(!StringUtils.isEmpty(serviceDiscoveryHost))
			{
				return this.serviceDiscoveryHost;
			} 
			else
			{
				return "http://" + backEndHost + ":" + backEndPort + "/" + backEndComponent;
			}
		}
		
	/*@Test
	public void check()
	{
		System.out.println("start");
		formBaseUrl();
	}
    */
    
    public void uploadToFTP(String localFilePath){
    	try {
    	  log.info("uploading partner-config at first SERVER");
    	  JSch jsch = new JSch();
  		  String filename = new File(localFilePath).getName();
  		  log.info("Remote file name: " + filename);
  		  com.jcraft.jsch.Session sftpsession = jsch.getSession(userName, server, 22);
  		  sftpsession.setPassword(password);
  		  Properties config = new Properties();
  		  config.setProperty("StrictHostKeyChecking", "no");
  		  sftpsession.setConfig(config);
  		  sftpsession.connect();
  		  ChannelSftp channel = (ChannelSftp) sftpsession.openChannel("sftp");
  		  channel.connect();
  		  
  		  String remoteFolder = new File(localFilePath).getParent() + "\\";
  		  remoteFolder = remoteFolder.replace("\\", "/");
  		  channel.cd(uploadPath);
  		  channel.put(new FileInputStream(new File(localFilePath)), filename);
  		  channel.disconnect();
  		  sftpsession.disconnect();
  		 
  		  uploadToFTPAtServerTwo(localFilePath);
  		  
  		 log.info("Thread sleep for 1 min.");
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		  
		 }
    
    
    public void uploadToFTPAtServerTwo(String localFilePath){
    	try {
    	  log.info("uploading partner-config at second SERVER");
    	  JSch jsch = new JSch();
  		  String filename = new File(localFilePath).getName();
  		  log.info("Remote file name: " + filename);
  		  com.jcraft.jsch.Session sftpsession = jsch.getSession(userName, server2, 22);
  		  sftpsession.setPassword(password);
  		  Properties config = new Properties();
  		  config.setProperty("StrictHostKeyChecking", "no");
  		  sftpsession.setConfig(config);
  		  sftpsession.connect();
  		  ChannelSftp channel = (ChannelSftp) sftpsession.openChannel("sftp");
  		  channel.connect();
  		  
  		  String remoteFolder = new File(localFilePath).getParent() + "\\";
  		  remoteFolder = remoteFolder.replace("\\", "/");
  		  channel.cd(uploadPath);
  		  channel.put(new FileInputStream(new File(localFilePath)), filename);
  		  channel.disconnect();
  		  sftpsession.disconnect();
  		 
  		  
  		  
  		  
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		  
		 }
    
    
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	
	public String getPartner() {
		return partner;
	}


	public void setPartner(String partner) {
		this.partner = partner;
	}
	
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	
	public void changePartner(String carrier,String product,String partner){
		
				config.setProperty("Carrier1",carrier);
				config.setProperty("Product1", product);
				config.setProperty("Partner1", partner);
				
				carrier = config.getProperty("Carrier1");
			 product =config.getProperty("Product1");
			 partner=config.getProperty("Partner1");
			 
	
			 System.out.println(carrier+partner+product);
				
	}
	
	
	public Connection getDbConnection(String driver, String url, String user, String password) {

		Connection connection = null;
		JDBCHelper acmt_jdbc = new JDBCHelper(driver, url, user, password);
		try {
			Class.forName(driver);
			connection = acmt_jdbc.getConnection();
		} catch (ClassNotFoundException e) {
			log.error("Check your library path included or not!!" + e);
		}

		return connection;
	}
	
	/**
	 * Method use to pass database name and get connection object of that database.
	 * @param db
	 * @return
	 */
	public Connection getDbConnection(String db) {
		
		Connection connection = null;
		JDBCHelper acmt_jdbc =null;
		String driver = null;
		if(db.equals(purchDb))
		{
			acmt_jdbc = new JDBCHelper(purchMgrDriver, purchMgrUrl, purchMgrUser, purchMgrPwd);
			driver = purchMgrDriver;
		}
		else if(db.equals(acctDb))
		{
			acmt_jdbc = new JDBCHelper(acctMgrDriver, acctMgrUrl, acctMgrUser, acctMgrPwd);
			driver = acctMgrDriver;
		}
		else if(db.equals(rightsDb))
		{
			acmt_jdbc = new JDBCHelper(rightsMgrDriver, rightsMgrUrl, rightsMgrUsr, rightsMgrPwd);
			driver = rightsMgrDriver;
		}
		
		try {
			Class.forName(driver);
			connection = acmt_jdbc.getConnection();
		} catch (ClassNotFoundException e) {
			log.error("Check your library path included or not!!" + e);
		}

		return connection;
	}

	public String checkEntryExistInTable(Connection connection, String tableName, String mappingFieldName,
			String mappingFieldValue) {

		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		StringBuilder sql = new StringBuilder();
		String status = null;
		tableName.toUpperCase();
		mappingFieldName.toUpperCase();
		
		sql.append("select * from ").append(tableName).append(" where ").append(mappingFieldName).append("='").append(mappingFieldValue).append("'");
		log.info("Verify entry exist in table :" + tableName + " For " + mappingFieldName+" = "+mappingFieldValue);
		try {
			pstmt = connection.prepareStatement(sql.toString().toUpperCase());
			resultSet = pstmt.executeQuery();
			if (resultSet !=null && resultSet.next()) {
				status = "exist";
			} else {
				status = "not_exist";
			}

		} catch (SQLException e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				log.error(e.toString());
			}
		}
		return status;
	}
	
	public ResultSet getBillingHistory(Connection connection, String tableName, String mappingFieldName,
			String mappingFieldValue) {
		
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		StringBuilder sql = new StringBuilder();
		String status = null;
		tableName.toUpperCase();
		mappingFieldName.toUpperCase();
		
		sql.append("select * from BILLING_HISTORY where SUBSCRIPTION_ID in (select SUBSCRIPTION_ID from SUBSCRIPTION where SUBSCRIPTION_USER_ID in (select SUBSCRIPTION_USER_ID from SUBSCRIPTION_USER where VUID ='").append(mappingFieldValue).append("'))");
		
		
		//sql.append("select * from ").append(tableName).append(" where ").append(mappingFieldName).append("='").append(mappingFieldValue).append("'");
		log.info(" : " + sql);
		try {
			pstmt = connection.prepareStatement(sql.toString().toUpperCase());
			resultSet = pstmt.executeQuery();

		}  catch (Exception e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		}
				
		return resultSet;
	}
	
	
	public ResultSet tableOperation(Connection connection,String selectFields, String tableName, String mappingFieldName,
			String mappingFieldValue) {

		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		StringBuilder sql = new StringBuilder();

		tableName.toUpperCase();
		mappingFieldName.toUpperCase();
		
		sql.append("select ").append(selectFields).append(" from ").append(tableName).append(" where ").append(mappingFieldName).append("='").append(mappingFieldValue).append("'");
		log.info("Verify rights details in " + tableName + " table : " + sql);
		try {
			
			pstmt = connection.prepareStatement(sql.toString().toUpperCase());
			resultSet = pstmt.executeQuery();

		} catch (SQLException e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		} finally {
			try {		

				pstmt = null;

			} catch (Exception e) {
				log.error(e.toString());
			}
		}

		return resultSet;
	}
	
	
	public ClientResponse webClientResponse(String externalId,PurchaseNotificationBatchRequest batchRequest) {
		
		String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";

		StringBuilder signaturePath= new StringBuilder();
		signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append("omnia").append("/").append(externalId).append("/purchases");
		RestRequest req = new RestRequest();
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature(signaturePath.toString(),carrier,epochTime);
		StringBuilder strurl= new StringBuilder();
		strurl.append("http://").append(host).append(":").append(port).append("/mobi-ssc-generic-billing-adapter/").append(signaturePath).append(".json?signature=").append(signature).append("&ts=").append(epochTime).append("&partner=").append("cspire");
		log.debug("Request URL : " + strurl);
		ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST).setHeaderParam("Content-Type", "application/json")
				.setContentType("application/json")
				/*.setPathParam("host", host)
				.setPathParam("port", port)*/
				.setPathParam("carrier", carrier)
				.setPathParam("product", product)
				.setPathParam("external_id", externalId)	
				.setQueryParam("ts", epochTime)
				.setQueryParam("partner", partner)
				.setQueryParam("signature", signature)
				.setBodyEntity(batchRequest)
				.setThrowEnabled(false).setHeaderParam("Accept", "application/json")
				.execute();

		log.debug("Request Header : " + req.getHeaderParams());
		


		return response;
	}
	
	
public ClientResponse webClientResponseMissingPartner(String externalId,PurchaseNotificationBatchRequest batchRequest) {
		
		String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";

		StringBuilder signaturePath= new StringBuilder();
		signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append(product).append("/").append(externalId).append("/purchases");
		RestRequest req = new RestRequest();
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature(signaturePath.toString(),carrier,epochTime);
		StringBuilder strurl= new StringBuilder();
		strurl.append("http://").append(host).append(port).append("/mobi-ssc-cspire-billing-adapter/").append(signaturePath).append(".json?signature=").append(signature).append("&ts=").append(epochTime).append("&partner=").append("cspire");
		log.debug("Request URL : " + strurl);
		ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST)
				.setContentType("application/json")/*.setPathParam("host", host).setPathParam("port", port)*/
				.setPathParam("carrier", carrier).setPathParam("product", product).setPathParam("external_id", externalId)
				.setQueryParam("signature", signature).setQueryParam("ts", epochTime)
				//.setQueryParam("partner","")
				.setBodyEntity(batchRequest)
				.setThrowEnabled(false)
				.execute();

		log.debug("Request Header : " + req.getHeaderParams());
		


		return response;
	}




public ClientResponse webClientResponseMissingSignature(String externalId,PurchaseNotificationBatchRequest batchRequest) {
	
	String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";

	StringBuilder signaturePath= new StringBuilder();
	signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append(product).append("/").append(externalId).append("/purchases");
	RestRequest req = new RestRequest();
	
	String epochTime = generateTimestamp();// Get from request URL
	//String signature=generateSignature(signaturePath.toString(),carrier,epochTime);
	StringBuilder strurl= new StringBuilder();
	strurl.append("http://").append(host).append(port).append("/mobi-ssc-cspire-billing-adapter/").append(signaturePath).append(".json?signature=").append("").append("&ts=").append(epochTime).append("&partner=").append("cspire");
	log.debug("Request URL : " + strurl);
	ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST)
			.setContentType("application/json")/*.setPathParam("host", host).setPathParam("port", port)*/
			.setPathParam("carrier", carrier).setPathParam("product", product).setPathParam("external_id", externalId)
			//.setQueryParam("signature", "")
			.setQueryParam("ts", epochTime).setQueryParam("partner", "cspire")
			.setBodyEntity(batchRequest).setThrowEnabled(false).execute();

	log.debug("Request Header : " + req.getHeaderParams());
	


	return response;
}
	
public ClientResponse webClientResponseMissingTimeStamp(String externalId,JSONObject batchRequest) {
	
	String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";

	StringBuilder signaturePath= new StringBuilder();
	signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append(product).append("/").append(externalId).append("/purchases");
	RestRequest req = new RestRequest();
	
	String epochTime = generateTimestamp();// Get from request URL
	String signature=generateSignature(signaturePath.toString(),carrier,epochTime);
	StringBuilder strurl= new StringBuilder();
	strurl.append("http://").append(host).append(port).append("/mobi-ssc-generic-billing-adapter/").append(signaturePath).append(".json?signature=").append(signature).append("&ts=").append("").append("&partner=").append("cspire");
	log.debug("Request URL : " + strurl);
	ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST)
			.setContentType("application/json")/*.setPathParam("host", host).setPathParam("port", port)*/
			.setPathParam("carrier", carrier).setPathParam("product", product).setPathParam("external_id", externalId)
			.setQueryParam("signature", signature).setQueryParam("ts","").setQueryParam("partner", "cspire")
			.setBodyEntity(batchRequest).setThrowEnabled(false)
			.execute();

	log.debug("Request Header : " + req.getHeaderParams());
	


	return response;
}

	public Object getDataFromJSONResponse(JSONObject jsonRes,String fieldName,int i){
		
		Object subStatus = null;
		
		try {
			JSONArray arr;
			arr = (JSONArray) jsonRes.getJSONArray("purchase_response");
			JSONObject val = (JSONObject) arr.get(i);
			subStatus = val.get(fieldName);
			log.debug("Purchases ID: " + subStatus);
		} catch (JSONException e1) {
			log.error(e1.toString());
		}
		
		return subStatus;
	}
	
	
	public Object getDataFromJSONArrayResponseForQueryAPI(JSONObject jsonres,String fieldname,String arrayname,int i){
		
Object fieldvalue = null;
		
		try {
			JSONArray arr;
			arr = (JSONArray) jsonres.getJSONArray(arrayname);
			JSONObject val = (JSONObject) arr.get(i);
			fieldvalue = val.get(fieldname);
			log.debug(fieldname +" : " + fieldvalue);
		} catch (JSONException e1) {
			log.error(e1.toString());
		}
		
		return fieldvalue;
		
		
	}
	
	public Object getDataFromJSONObjectResponseQueryAPI(JSONObject jsonres,String fieldname,String objname,int i){
		
		Object fieldvalue = null;
		
		JSONObject val;
		try {
			val = (JSONObject) jsonres.get(objname);
			fieldvalue = val.get(fieldname);
			log.debug(fieldname +" : " + fieldvalue);
			
		} catch (JSONException e) {
			log.info(e.getMessage());
		}
		
		
		return fieldvalue;
	}
	
	/**
	 * method use for generate random externalId.
	 * @return
	 */
	public String getRandomExternalId()
	{
		return String.valueOf(rand.nextInt(99999999));
	}
	
	

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	            char[] hexChars = new char[bytes.length * 2];
	            for (int j = 0; j < bytes.length; j++) {
	                    int v = bytes[j] & 0xFF;
	                    hexChars[j * 2] = hexArray[v >>> 4];
	                    hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	            }
	            return new String(hexChars);
	}
	
	public static String generateTimestamp(){
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStmpStr = new String(Long.valueOf(timestamp.getTime()).toString().substring(0,10));
		//System.out.println(timeStmpStr);
		return timeStmpStr;
	}	

	
	public  String generateSignature(String urlPath,String partnerId,String timeStamp)
	{
		String hash =null;
		try {  
			
			
            String message = urlPath + ":" + partnerId + ":" + timeStamp;// "url_path:partner_id:epoch_time";
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(Secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            hash = bytesToHex(sha256_HMAC.doFinal(message.getBytes()));    
        }
		catch (Exception e) {
            System.out.println("Error");
		}
		return hash;
	}
	
	public JSONObject webResponse(String externalid,JSONObject reqObj) {
		
		JSONObject jsonObj=null;
				String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";
			StringBuilder signaturePath = new StringBuilder();
			signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append(product).append("/").append(externalid).append("/purchases");
			RestRequest req = new RestRequest();
			String epochTime = generateTimestamp();// Get from request URL
			String signature = generateSignature(signaturePath.toString(), carrier, epochTime);
			StringBuilder strurl = new StringBuilder();
			strurl.append("http://").append(host + ":").append(port).append("/mobi-ssc-generic-billing-adapter/").append(signaturePath).append(".json?signature=").append(signature).append("&ts=").append(epochTime).append("&partner=").append("ezv");
			log.info("Request URL : " + strurl);
			ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST).setContentType("application/json")/*.setPathParam("host", host).setPathParam("port", port)*/.setPathParam("carrier", carrier).setPathParam("product", product)
					.setPathParam("external_id", externalid).setQueryParam("signature", signature).setQueryParam("ts", epochTime).setQueryParam("partner", partner).setBodyEntity(reqObj).setThrowEnabled(false).execute();
			log.info("Request Header : " + req.getHeaderParams());
			
			log.info("Recieved Response code : " + response.getStatus());
			
			log.info("Verifying response : " );
			if (response.getStatus()!=200) {
				log.info("Purchase Respone" + response);
				Assert.fail("Recieved Response Error code " +response.getStatus() );
			}
			jsonObj = response.getEntity(JSONObject.class);
	
		return jsonObj;
		
	}

	public JSONObject webUserResponse(String externalid, JSONObject reqObj, String product, String version) {

		JSONObject jsonObj = null;
		String path = formBaseUrl()+"/partner/v1/notification/purchase/{carrier}/{product}/{external_id}/purchases.json";
		StringBuilder signaturePath = new StringBuilder();
		signaturePath.append("partner/v1/notification/purchase/").append(carrier).append("/").append(product).append("/").append(externalid).append("/purchases");
		RestRequest req = new RestRequest();
		String epochTime = generateTimestamp();// Get from request URL
		String signature = generateSignature(signaturePath.toString(), carrier, epochTime);
		StringBuilder strurl = new StringBuilder();
		strurl.append("http://").append(host + ":").append(port).append("/mobi-ssc-generic-billing-adapter/").append(signaturePath).append(".json?signature=").append(signature).append("&ts=").append(epochTime).append("&partner=").append("ezv");
		log.info("Request URL : " + strurl);
		ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.POST).setContentType("application/json")/*.setPathParam("host", host).setPathParam("port", port)*/.setPathParam("carrier", carrier).setPathParam("product", product)
				.setPathParam("external_id", externalid).setQueryParam("signature", signature).setQueryParam("ts", epochTime).setQueryParam("partner", partner).setBodyEntity(reqObj).setThrowEnabled(false).execute();
		log.info("Request Header : " + req.getHeaderParams());

		log.info("Recieved Response code : " + response.getStatus());

		log.info("Verifying response : ");
		if (response.getStatus() != 200) {
			log.info("Purchase Respone" + response);
			Assert.fail("Recieved Response Error code " + response.getStatus());
		}
		jsonObj = response.getEntity(JSONObject.class);

		return jsonObj;

	}
	
	
public HttpURLConnection webMissingParameterResponse(String externalid,JSONObject reqObj,String partner,String signature,String timestamp) {
		

	JSONObject jsonObj=null;
	
	HttpURLConnection conn = null;
	
	try {

		
		
	
		URL url = new URL(
				formBaseUrl()+"/partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases.json?partner="+partner+"&ts="+timestamp+"&signature="+signature);
		 conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-Type", "application/json");

		/*OutputStream os = conn.getOutputStream();
		os.write(reqObj.toString().getBytes());
		os.flush();*/
		log.info(url);
		
		
	} catch (MalformedURLException e) {

		e.printStackTrace();
	} catch (IOException e) {

		e.printStackTrace();

	}
	return conn;
		
	}
	
public HttpURLConnection webStatusResponse(String externalid,JSONObject reqObj) throws ClientProtocolException, IOException, JSONException{
		
		JSONObject jsonObj=null;
		
		HttpURLConnection conn = null;
		
		try {

			
			
			String epochTime = generateTimestamp();// Get from request URL
			String signature=generateSignature("partner/v1/notification/purchase/"+carrier+"/"+product+"/"+externalid+"/purchases",carrier,epochTime);
			
			URL url = new URL(
					formBaseUrl()+"/partner/v1/notification/purchase/"+carrier+"/"+product+"/"+externalid+"/purchases.json?partner="+partner+"&ts="+epochTime+"&signature="+signature);
			 conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
	
			OutputStream os = conn.getOutputStream();
			os.write(reqObj.toString().getBytes());
			os.flush();
			log.info(url);
			
			
		} catch (MalformedURLException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();

		}
		return conn;
	
	}
	

public HttpURLConnection webUserStatusResponse(String externalid,JSONObject reqObj,String product) throws ClientProtocolException, IOException, JSONException{
	
	JSONObject jsonObj=null;
	
	HttpURLConnection conn = null;
	
	try {

		
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature("partner/v1/notification/purchase/"+carrier+"/"+product+"/"+externalid+"/purchases",carrier,epochTime);
		
		URL url = new URL(
				formBaseUrl()+"/partner/v1/notification/purchase/"+carrier+"/"+product+"/"+externalid+"/purchases.json?partner="+partner+"&ts="+epochTime+"&signature="+signature);
		 conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

		OutputStream os = conn.getOutputStream();
		os.write(reqObj.toString().getBytes());
		os.flush();
		log.info(url);
		
		
	} catch (MalformedURLException e) {

		e.printStackTrace();
	} catch (IOException e) {

		e.printStackTrace();

	}
	return conn;

}
	
public JSONObject webQuantityResponse(String externalid,JSONObject reqObj) throws ClientProtocolException, IOException, JSONException{
	
	JSONObject jsonObj=null;
	
	
	
	try {

		
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature("partner/v1/notification/purchase/directlink/paytv/"+externalid+"/purchases",carrier,epochTime);
		
		URL url = new URL(
				formBaseUrl()+"/partner/v1/notification/purchase/directlink/paytv/"+externalid+"/purchases.json?partner=directlink&ts="+epochTime+"&signature="+signature);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");

		OutputStream os = conn.getOutputStream();
		os.write(reqObj.toString().getBytes());
		os.flush();
		log.info(url);
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
	String jsonres = br.getClass().toString();
		
	String res = "";
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {

			System.out.println(output);
			res+=output;
		}
	
		jsonObj = new JSONObject(res);
		
		conn.disconnect();
		
	} catch (MalformedURLException e) {

		e.printStackTrace();
	} catch (IOException e) {

		e.printStackTrace();

	}
	return jsonObj;
}

public JSONObject webQueryAPIResponse(String externalid,JSONObject reqObj) throws ClientProtocolException, IOException, JSONException{
	
	JSONObject jsonObj=null;
	
	
	
	try {

		
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
		
		URL url = new URL(
				formBaseUrl()+"/partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases.json?partner="+partner+"&ts="+epochTime+"&signature="+signature);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-Type", "application/json");
/*
		OutputStream os = conn.getOutputStream();
		os.write(reqObj.toString().getBytes());
		os.flush();*/
		log.info(url);
		if (conn.getResponseCode() != 200) {
			Assert.fail("Response code Error" + conn.getResponseCode() );
			
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
	String jsonres = br.getClass().toString();
		
	String res = "";
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {

			System.out.println(output);
			res+=output;
		}
	
		jsonObj = new JSONObject(res);
		
		conn.disconnect();
		
	} catch (MalformedURLException e) {

		e.printStackTrace();
	} catch (IOException e) {

		e.printStackTrace();

	}
	return jsonObj;
	
}


public JSONObject webQueryAPIResponseWithStatus(String externalid,JSONObject reqObj,String status) {
	
	JSONObject jsonObj=null;
	
	
	
	try {

		
		
		String epochTime = generateTimestamp();// Get from request URL
		String signature=generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
		
		URL url = new URL(
				formBaseUrl()+"/partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases.json?partner="+partner+"&ts="+epochTime+"&signature="+signature+"&status="+status);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-Type", "application/json");
/*
		OutputStream os = conn.getOutputStream();
		os.write(reqObj.toString().getBytes());
		os.flush();*/
		log.info(url);
		if (conn.getResponseCode() != 200) {
			Assert.fail("Response code Error" + conn.getResponseCode() );
			
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
	String jsonres = br.getClass().toString();
		
	String res = "";
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {

			System.out.println(output);
			res+=output;
		}
	
		
			jsonObj = new JSONObject(res);
		
		
		conn.disconnect();
		
	} catch (Exception e) {

		log.info(e.getMessage());
	} 
	return jsonObj;
	
}

public ClientResponse healthCheckResponse() {
	
	String path = formBaseUrl()+"/monitoring/health";

	StringBuilder signaturePath= new StringBuilder();
	RestRequest req = new RestRequest();
	
	ClientResponse response = req.setUrl(path.toString()).setMethod(HttpMethodType.GET)
			/*.setPathParam("host", host)
			.setPathParam("port", port)*/
			.setThrowEnabled(false).setHeaderParam("Accept", "application/json")
			.execute();

	log.debug("Request Header : " + req.getHeaderParams());
	


	return response;
}

}
