package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;


//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire

/**
*
* @author 
*
* @test.suite.description
* FipsCode  
*
* @test.prerequisite
* Process_FIPS_Code
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6693
*/
public class FipsCodeTestCasesType2 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "2_TV2STRM";
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	
		ConfigReaderHelper configreader = new ConfigReaderHelper("Fips_Code_Type2.properties");
	
		@BeforeClass
		public static void initialConditions(){
		
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type2/partner-config");
			
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
			
		}
		
		
		
		private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
			public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With the configuration as "fips.code.config = extenal_system_zip" user created one purchase by sending the ZIP code in the purchase request gives call to external Service and inserts FIPS code received from external service. 
	
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * fips.code.config=external_system_zip
		
		
		
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543674
	 */
	@SuppressWarnings("unused")
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_003 (){
		
		log.info("Starting test case 3rd");
		generatePurchaseRequest();
		log.info("Checking offer type");
		
		try {
		log.info("Checking Partner Config");	
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", "TV2JXN");
		
				
			}else{
				purchase.put("product_id", "4_TV2STOR");
			}
			
			
			account.put("zip_code", "84036");
			
			
			ResultSet resultSet = null;
	
		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
	
	
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
	
			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}
	
			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			
			
			log.info("Verifying the fips code");
			assertNotNull(fips);
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 3rd....");
		}
		
		log.info("ending test case 3rd....");
		
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	Create one purchase for user with the configuration as fips.code.config=external_system_zip and do not provide the ZIP code in the account object, does not add FIPS code and gives message to user 
	
	* 
	* @test.verification Verifies the API returns status code 400.
	* 
	* fips.code.config=external_system_zip
	* region.config=default
	
			
	* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543678
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_007(){
	
		log.info("Starting test case 7th");
		generatePurchaseRequest();
		log.info("Checking offer type");
		ResultSet resultSet = null;
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
			
		
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking Partner Config");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
	
			//account.put("zip_code", "9999");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 7th....");
		}
	}else{
		log.info("Carrier.fips.code.config is not equal to external_system_zip");
		}
		log.info("ending test case 7th....");
	}

	
	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	Create one purchase for user with the configuration as fips.code.config=external_system_zip and do not provide the ZIP code in the account object, does not add FIPS code and gives message to user 
	
	* 
	* @test.verification Verifies the API returns status code 400.
	* 
	* fips.code.config=external_system_zip
	* region.config=default
	
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/1930826
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_008(){
	
		log.info("Starting test case 8th");
		generatePurchaseRequest();
		log.info("Checking offer type");
		ResultSet resultSet = null;
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
			
		
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking Partner Config");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
	
			account.put("zip_code", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 8th....");
		}
	}else{
		log.info("Carrier.fips.code.config is not equal to external_system_zip");
		}
		log.info("ending test case 8th....");
	}
	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	Create one purchase with the configuration as fips.code.config=external_system_zip and provide any dummy zip code in the accoiunt object, does not add fips code in the partner account table and provides proper warning message to user.
	
	* 
	* @test.verification Verifies the API returns status code 500.
	* 
	* fips.code.config=external_system_zip
	* region.config=default
	
			
	* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543695
	 */
	
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_009(){
	
		log.info("Starting test case 9th");
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
	
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
			
		
		try {
		
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
	
			account.put("zip_code", "ABCD");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(500, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 9th....");
		}
	}else{
		log.info("Carrier.fips.code.config is not equal to external_system_zip");
		}
		log.info("ending test case 9th....");
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	With the fips.code.config=partner/external_system_zip/external_system_county_state/default if user not send Action in incoming API, not create the purchase and return HTTP 400. 	* @test.verification Verifies the API returns status code 200.
	* 
	* 
	* region.config=default
	fips.code.config=external_system_zip
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151074
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_017(){
		
		log.info("Starting test case 17");
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
	
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
	
			account.put("zip_code", "84036");
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 17th....");
		}
		log.info("ending test case 17th....");
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
		With the fips.code.config=partner/external_system_zip/external_system_county_state/default if user sending invalid purchase status in incoming API, not create the purchase and return HTTP 400. 
	* 
	* 
	* region.config=default
	fips.code.config=external_system_zip/
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151075
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_018(){
		
		log.info("Starting test case 18");
		generatePurchaseRequest();
		
		
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
			
			log.info("Inserting Invalid staus");
			account.put("status", "ABCD");
			account.put("zip_code", "84836");
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 18th....");
		}
		log.info("ending test case 18th....");
	}
}

