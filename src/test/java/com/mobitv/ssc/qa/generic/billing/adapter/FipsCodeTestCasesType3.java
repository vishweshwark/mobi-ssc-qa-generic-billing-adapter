package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire
/**
*
* @author 
*
* @test.suite.description
* FipsCode 
*
* @test.prerequisite
* Process_FIPS_Code
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6693
*/
public class FipsCodeTestCasesType3 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "2_TV2STRM";
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	
	ConfigReaderHelper configreader = new ConfigReaderHelper("Fips_Code_Type3.properties");
		
		
		@BeforeClass
		public static void initialConditions(){
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type3/partner-config");
			
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
		}
		
		private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
			public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	/**
		 * @throws Exception
		 * 
		 * @test.type functional
		 * 
		 * @test.description 
	 	  With the configuration set as "external_system_county_state" and user creates one purchase by sending the County and State to external service. 
	
		 * 
		 * @test.verification Verifies the API returns status code 200.
		 * 
		 * fips.code.config=external_system_county_state
			
		 * 
		 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543675
		 */
	
	
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_004(){
		
		log.info("Starting test case 4th");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		try {
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_county_state")) {
			
			ResultSet resultSet = null;
			
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "4",purchase);
					purchase.put("product_id", "TV2STRM");
	
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "4_TV2STRM");
			}
			
			log.info("Calling to create an Purchase without fips code");
			account.remove("fips_code");
			account.put("region","TESTREGION");
			account.put("county","Summit");
			account.put("state","UT");
			JSONObject purchase1 = getPurchase();
			
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "4",purchase1);
						purchase1.put("product_id", "TV2STOR");
		
					} catch (Exception e) {
						log.error(e.getMessage());
					} 
					}else{
						purchase1.put("product_id", "4_TV2STOR");
					}
				
			
			purchasearray.put(purchase1);
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
	
			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data
	
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
	
			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}
	
			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			{
				JSONObject purchase2 = getPurchase();
				
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase2);
						purchase2.put("product_id", "TV2STRM");
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}else{
					purchase2.put("product_id","1_TV2STRM");
				}
				
				purchasearray.put(purchase2);
				
				JSONObject purchase3 = getPurchase();
				
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase3);
						purchase3.put("product_id", "TV2STOR");
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}else{
					purchase3.put("product_id","2_TV2STOR");
				}
				
				purchasearray.put(purchase3);
				
				purchase.put("action", "cancel");
				purchase1.put("action", "cancel");
				purchase2.put("action", "create");
				purchase3.put("action", "create");
				
				account.put("fips_code", "test_f_code1cc");
				
				 jsonRes = genericUtil.webResponse(externalid, reqobj);
	
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				 i = 0;
				subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
	
				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				 subscription_user_id = "";
				 Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}
	
				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				 account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				 account_id_guid = "";
				 account_id="";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
					account_id=resultSet.getString("ACCOUNT_ID");
				}
				
				partnername="";
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				 fips = "";
				if (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					fips = resultSet.getString("PARTNER_ACCOUNT_ID");
				}
				log.info("Verifying Fips Code Value not null");
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);
				assertNotNull(fips);
				
			}
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_county_state");
			
		}
	
}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 4th....");
		}
		
		log.info("ending test case 4th....");
		
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	 Create one purchase for user with the configuration as fips.code.config=external_system_county_state does not set fips code provided by partner in the account object. 
	
	* 
	* @test.verification Verifies the API returns status code 200.
	* 
	* fips.code.config=external_system_county_state
			
	* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543677
	 */
	
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_006(){
		
		log.info("Starting test case 6");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		try {
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_county_state")) {
			
			ResultSet resultSet = null;
			
			generatePurchaseRequest();
			log.info("Checking Partner Config");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase without fips code");
			account.put("region","TESTREGION");
			account.put("county","Utah");
			account.put("state","UT");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
	
			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data
	
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
	
			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}
	
			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_county_state");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 6th....");
		}
		
		log.info("ending test case 6th....");
		
	}

	/**
			* @throws Exception
			 * 
			* @test.type functional
		    * 
			* @test.description 
			Create one purchase with the configuration as fips.code.config=external_system_county_state and provide dummy cnty/st code in the account object, add fips code in partner accnt table as per the cnty/state and provides proper warning message to user. 
	
			* 
			* @test.verification Verifies the API returns status code 200.
			* 
			* fips.code.config=external_system_county_state
			* region.config=default
	
					
			* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543696
			 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_010(){
		
		log.info("Starting test case 10th");
		generatePurchaseRequest();
		log.info("Checking Partner Config");
	
		
		try {
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_county_state")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase without fips code");
			account.put("region","TESTREGION");
			account.put("county","Summit");
			account.put("state","UT");
			JSONObject purchase1 = getPurchase();
			purchase1.put("product_id", product_id5);
			purchasearray.put(purchase1);
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
	
			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data
	
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
	
			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}
	
			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_county_state");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 10th....");
		}
		
		log.info("ending test case 10th....");
		
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
	With the fips.code.config=external_system_county_state user not sending the state or county in the API call then purchase will not be created and user should receive HTTP 400. 	* 
	* 
	* region.config=default
	fips.code.config=external_system_county_state
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151079
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_019(){
		
		log.info("Starting test case 19");
		generatePurchaseRequest();
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_county_state")) {
			
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");
	
			} else {
				log.info("region.code.config=default");
			}
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
	
			account.put("zip_code", "84836");
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 19th....");
		}
		}else{
		log.info("Carrier.fips.code.config is not equal to external_system_county_state");
		}
		log.info("ending test case 19th....");
	}
}

