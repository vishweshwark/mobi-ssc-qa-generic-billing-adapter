package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;


//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire
/**
*
* @author 
*
* @test.suite.description
* FipsCode
*
* @test.prerequisite
* Process_FIPS_Code
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6693
*/
public class FipsCodeTestCasesType1 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "2_TV2STRM";
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("Fips_Code_Type1.properties");
	
	@BeforeClass
	public static void initialConditions(){
	
		genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type1/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
	}
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 	With the configuration as "fips.code.config = default", verify the FIPS code is taken as is from partner-config.property file when new subscription is created. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * provider=cspire

		cspire.user.identifiers=omnia_id:external_id
		cspire.fips.code.config=default
		cspire.fips.code.default=49009
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		cspire.feature.offer.handling=update
		cspire.feature.offers=TV2STOR,TV2STRM
		cspire.version=1.0
		cspire.product=paytv
		
		partner security config

		cspire.secret_key=cspire#~SEPARATOR~#0f0d4f0e3d501af466f1d59831a7bbc440292c6d
		cspire.timestamp_validity=120
		cspire.encrypt_algo=HmacSHA256
		cspire.auth.signature.validation=true
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5543673
	 */

	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_002 (){
		try {
		log.info("Starting test case 2nd");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", "TV2COL");

			
		}else
		{
			purchase.put("product_id", "HBO");
		}
		
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
		
			//account.put("region","TESTREGION");
			//purchase.put("product_id", "TV2JXN");
			//account.put("county","Summit");
			
		ResultSet resultSet = null;

		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			while (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				if ("fips_code".equals(partnername)) {
					
					fips = resultSet.getString("PARTNER_ACCOUNT_ID");
				}
				
			}
			assertEquals(configreader.getConfigValue(carrier+".fips.code.default"),fips);
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to Default");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 2nd....");
		}
		
		log.info("ending test case 2nd....");
		
	}
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
			/**
	* @throws Exception
	 * 
	* @test.type functional
    * 
	* @test.description 
		With the fips.code.config=default user sends create request creates the purchase and adds default fips_code configured in the partner.config. 
	* 
	* @test.verification Verifies the API returns status code 200.
	* 
	* fips.code.config=default
	* region.config=default

			
	* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544759
	 */
	
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_015(){
		
		log.info("Starting test case 15");
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		try {
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase without fips code");
			account.put("region","TESTREGION");
			account.put("county","Summit");
			account.put("state","UT");
			JSONObject purchase1 = getPurchase();
			purchase1.put("product_id", product_id5);
			purchasearray.put(purchase1);
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			log.info("Verifying the Creation of of purchase in DB");
			assertEquals("exist", subscriptionUserStatus);
			assertNotNull(fips);
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to Default");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 15th....");
		}
		
		log.info("ending test case 15th....");
		
	}
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
}

