package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseInfo;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;
import com.mobitv.platform.partner.dto.Purchases;
/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_SSC_9696_SupportActiveAccountStatus_Sprint73
*
* @test.prerequisite
* Support_Active_As_Account Status
*
* @test.reference
*https://testrail.mobitv.corp/index.php?/runs/view/7081
*/
public class SupportActiveAcctStatusTest extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("SupportActiveStatus.properties");


	  @BeforeClass public static void initialConditions(){
	  
	 genericUtil.uploadToFTP(
	 "./src/test/resources/config/SupportActiveAccount/partner-config");
	 
	  
	 try { Thread.sleep(60000); } catch (InterruptedException e1) { 
	  e1.printStackTrace(); } }
	 

	private void generatePurchaseRequest() {

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();

			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With Account that does not exist create Purchases for
	 *                   user by passing "status"="active", verify creates
	 *                   purchases and account status is set as "enabled".
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6322973
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_001() {
		try {
			log.info("Starting test case 1");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("Ending test case 1");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With Suspended Account status, send account object
	 *                   without any purchases and by passing "status"="active"
	 *                   in account object, verify account status is set as
	 *                   "enabled".
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6322974
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_002() {
		try {

			log.info("Starting test case 2");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Call with Active Account Status");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("API call with Suspend as Status");

			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Active as Status");

			account.remove("status");
			account.put("status", "active");
			// reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			account_id_guid = "";
			accountStatus = "";

			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("Ending test case 2");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	//test case was wrong have corrected
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With Disabled Account status, send account object
	 *                   without any purchases and by passing "status"="active"
	 *                   in account object, verify account status is not set to
	 *                   "enabled".
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2549855
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_003() {
		try {

			log.info("Starting test case 3");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("API call with Diconnect as Status");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Active as Status");

			account.remove("status");
			account.put("status", "active");
			// reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			account_id_guid = "";
			accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("Starting test case 3");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description Send Account Status as Active without sending any
	 *                   Purchase in the request, creates the Account and sets
	 *                   Account Status as "enabled"
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6322976
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_004() {
		try {
			log.info("Starting test case 4");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			reqobj.remove("purchase");

			log.info("Request without purchase object.");

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			JSONObject accountObj = jsonRes.getJSONObject("account");
			String status = accountObj.getString("status");

			log.info("verifying status");
			log.info(" Status " + status);
			assertEquals("ENABLED", status);

			log.info("Starting test case 4");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With existing user having suspended account status Pass
	 *                   an invalid account status in account object without any
	 *                   purchases and check if account status remains
	 *                   unchanged.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6323117
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_005() {
		try {
			log.info("Starting test case 5");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Suspend as Status");

			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Invalid as Status");

			account.remove("status");
			account.put("status", "activate");
			// reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());

			log.info("Starting test case 5");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With existing account having suspended account status
	 *                   and cancelled purchases, re-activating the acct with
	 *                   acct status as active, sets the status as enabled.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6342020
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_006() {
		try {
			log.info("Starting test case 6");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("API call with Suspend as Status");

			account.remove("status");
			account.put("status", "Suspend");
			purchase.remove("action");
			purchase.put("action", "cancel");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Active as Status");

			account.remove("status");
			account.put("status", "active");
			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			account_id_guid = "";
			accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			log.info("Starting test case 6");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With existing account having "suspended" account status
	 *                   and active purchases, re-activating the acct with acct
	 *                   status as active, sets the status as enabled.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6342034
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_007() {
		try {
			log.info("Starting test case 7");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("API call with Suspend as Status");

			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Active as Status");

			account.remove("status");
			account.put("status", "active");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			account_id_guid = "";
			accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);

			log.info("Starting test case 7");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With existing account having "active" account status
	 *                   and now sending account status as "active" should not
	 *                   update anything related to account status
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6354840
	 */
	@Test
	public void SSC_Directlink_GenericBillingAdapter_ActAccnt_008() {
		try {
			log.info("Starting test case 8");
			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			log.info("API call with Active as Status");

			account.remove("status");
			account.put("status", "active");

			reqobj.remove("purchase");

			jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			JSONObject accountObj = jsonRes.getJSONObject("account");
			String status = accountObj.getString("status");

			log.info("verifying status");
			log.info(" Status " + status);
			assertEquals("ENABLED", status);

			log.info("Starting test case 8");

		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	public void addQuantity(String name, String value, JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
}
