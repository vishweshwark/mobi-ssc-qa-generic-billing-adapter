package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mortbay.log.Log;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;

import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire

/**
*
* @author 
*
* @test.suite.description
* Feature Offer 
*
* @test.prerequisite
* Generic Billing Adapter_FeatureOffer_Sprint67
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6656
*/
public class FeatureOfferTestCasesType3 extends MobiTestBase {

	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "4_TV2STRM";
	private final String product_id1 = "1_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = "T5476848";
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	//ConfigReaderHelper configreader = new ConfigReaderHelper();
	ConfigReaderHelper configreader = new ConfigReaderHelper("F_OfferType3_cspire.properties");

	@BeforeClass
	public static void initialConditions(){
	
		genericUtil.uploadToFTP("./src/test/resources/config/F_OfferType3_cspire/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
	}
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

		}

	}
	/**
	* @throws Exception
	 * 
	* @test.type functional
    * 
	* @test.description 
	With an active account status and feature.offer.config = quantity and user sends non numeric/less than 0 in the quantity paramater then purchase should not be created and should return HTTP 400. 
	* 
	* 
	* 	.region.config=default
		.fips.code.config=partner
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151076
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_018(){
		
		log.info("Starting test case 18");
		generatePurchaseRequest();
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");

			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "0");
					purchase.put("product_id", product_id2);
	
			}

			account.put("zip_code", "84836");
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}

		log.info("ending test case 1st....");
	}	
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	public JSONObject getPurchaseWithExpiry() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("expires_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	public void addQuantity(String name, String value) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			purchase.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
