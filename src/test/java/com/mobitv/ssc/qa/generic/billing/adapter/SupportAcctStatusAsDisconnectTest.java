package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;

public class SupportAcctStatusAsDisconnectTest extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("SupportActiveStatus.properties");

	@BeforeClass
	public static void initialConditions() {

		genericUtil.uploadToFTP("./src/test/resources/config/SupportActiveAccount/partner-config");
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	private void generatePurchaseRequest() {

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();

			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	//done changes as per test case
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the disconnected account, if user sends request for reactivation of purchases with account status as null, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708639
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_001() {
		try {
			log.info("Starting test case 1");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			
			log.info("API call with null as Status");
			account.remove("status");
			String	status = null;
			account.put("status",status );
			//reqobj.remove("purchase");


			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			log.info("Ending test case 1");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * @test.description : With the disconnected account, if user sends request for reactivation of purchases with account status as active, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/6708640
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_002() {
		try {
			log.info("Starting test case 2");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Active as Status");
			generatePurchaseRequest();
			account.remove("status");
			account.put("status", "active");

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Ending test case 2");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional

	 * @test.description   With the disconnected account, if user sends request for reactivation of purchases with account status as empty, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708641
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_003() {
		try {
			log.info("Starting test case 3");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Empty as Status");
			generatePurchaseRequest();
			account.remove("status");
			account.put("status", "");

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Starting test case 3");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :With the disconnected acct, if user purchases acct with acct status as null & sends request for reactivation of purchases with acct status as empty, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708642
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_004() {
		try {
			log.info("Starting test case 4");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Null as Status");
			generatePurchaseRequest();
			String status = null;
			account.remove("status");
			account.put("status", status);

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Starting test case 4");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :  With the disconnect account and cancelled purchases, if user sends request for reactivating purchases with account status as null, verify response returns 200
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference : https://testrail.mobitv.corp/index.php?/tests/view/6708643
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_005() {
		try {
			log.info("Starting test case 5");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status and action as cancel");

			account.remove("status");
			account.put("status", "disconnect");
			purchase.remove("action");
			purchase.put("action", "cancel");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Null as Status");
			generatePurchaseRequest();
			String status = null;
			account.remove("status");
			account.put("status", status);

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Starting test case 5");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 

	 * @test.description: With the disconnect account and active purchases, if user sends request for reactivating account with only account object and account status as null, verify response returns 200.

	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference: https://testrail.mobitv.corp/index.php?/tests/view/6708644
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_006() {
		try {
			log.info("Starting test case 6");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status and action as cancel");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Null as Status");

			String status = null;
			account.remove("status");
			account.put("status", status);

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Ending Test Case 6th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With disconnect acct & active purchases, if user sends req. for reactivating acct with empty purchase object, acct object & acct status as null, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708645
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_007() {
		try {
			log.info("Starting test case 7");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status and action as cancel");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Null as Status");

			String status = null;
			account.remove("status");
			account.put("status", status);
			reqobj.remove("purchase");
			JSONArray emptyPurchase = new JSONArray();
			reqobj.put("purchase", emptyPurchase);

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Ending Test Case 7th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description    With existing user having disconnected account status Pass an invalid account status in account object with empty purchases and account/purchase statuses should remain unchanged.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708646
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_008() {
		try {
			log.info("Starting test case 8");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status and action as cancel");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Null as Status");

			String status = "invalid";
			account.remove("status");
			account.put("status", status);
			reqobj.remove("purchase");
			JSONArray emptyPurchase = new JSONArray();
			reqobj.put("purchase", emptyPurchase);

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());

			log.info("Ending Test Case 8th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	//changes done as per new build

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the disconnected act, if user sends request for suspended the account, verify response returns 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708647
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_009() {
		try {
			log.info("Starting test case 8");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status and action as cancel");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with Suspend as Status");

			String status = "Suspend";
			account.remove("status");
			account.put("status", status);
			reqobj.remove("purchase");

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Ending Test Case 9th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description: For an existing user with terminated account, if user requests to update zip code verify response returns success and account should be in active state.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference: https://testrail.mobitv.corp/index.php?/tests/view/7319174
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_010() {
		try {
			log.info("Starting test case 10th");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status ");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with zip_code as update");

			String status = "";
			account.remove("status");
			account.put("status", status);
			purchase.put("zip_code", "12345");

			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			log.info("Ending Test Case 10th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description:  For an existing user with terminated account, if user requests to cancel purchase with account status as terminate, verify response returns failure. 
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference: https://testrail.mobitv.corp/index.php?/tests/view/7319175
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_011() {
		try {
			log.info("Starting test case 11th");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status ");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with zip_code as update");

			String status = "disconnect";
			account.remove("status");
			account.put("status", status);
			//purchase.put("zip_code", "12345");
			
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			reqobj.put("purchase", purchase);
			
			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());

			log.info("Ending Test Case 11th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description: For an existing user with terminated account, if user requests to cancel purchase with account status as suspend, verify response returns failure. 
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference: https://testrail.mobitv.corp/index.php?/cases/view/3859311
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Disconnect_account_012() {
		try {
			log.info("Starting test case 12th");

			generatePurchaseRequest();

			ResultSet resultSet = null;

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);

			log.info("API call with Disconnect as Status ");

			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());

			//
			log.info("API call with zip_code as update");

			String status = "suspend";
			account.remove("status");
			account.put("status", status);
			//purchase.put("zip_code", "12345");
			
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			reqobj.put("purchase", purchase);
			
			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());

			log.info("Ending Test Case 10th");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
}
