package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv

/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_Update Partner Account_Sprint 70
*
* @test.prerequisite
* Update Partner Account
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6851
*/
public class UpdatePartnerAccountTest2 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("WithoutPurchaseObject_Type1.properties");
	

	@BeforeClass
	public static void initialConditions(){
	
		genericUtil.uploadToFTP("./src/test/resources/config/WithoutPurchaseObject_Type1/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
	}
	
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update account_status as disconnect, verify response returns 200 and account_status = disabled, and account table is updated with new value  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=durectlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5803391
	 */
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_001  (){
		
		String extid = "";
		log.info("Starting test case 1st");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", "84084");
			
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);

			}


			log.info("Calling without purchase object and with Disconnect Status");
			{
				account.put("status", "disconnect");
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("disabled", status);

			}
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 1st....");
		}
		
		log.info("ending test case 1st....");
		
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'account_status' from suspend to disconnect, verify resp. returns 200 and account_status as disabled in acct. table is upd. with new value   
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5807496
	 */
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_002  (){
		try{
			String extid = "";
			log.info("Starting test case 1st");
			generatePurchaseRequest();
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
			
				account.put("zip_code", "84084");
				
				ResultSet resultSet = null;
		
		JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
		
		
		int i = 0;
		String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

		log.info("verifying record in subscription table.");
		resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
		String subscription_user_id = "";
		String Expire_Date = "";
		if (resultSet.next()) {
			log.info("Fetching Subscription_User_ID from record");
			subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
			Expire_Date = resultSet.getString("EXPIRES_DATE");
			log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
		}

		log.info("verifying record in subscription user table.");
		resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
		String account_id = "";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id = resultSet.getString("ACCOUNT_ID");
			log.info("ACCOUNT_ID=>" + account_id);
		}
		log.info("verifying record in account table.");
		resultSet = commonTableValidators.getDataFromAccountTable(account_id);
		String account_id_guid = "";
		 account_id="";
		 String account_status="";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id_guid = resultSet.getString("ACCOUNT_GUID");
			log.info("ACCOUNT_GUID=>" + account_id_guid);
			account_id=resultSet.getString("ACCOUNT_ID");
		
			account_status = resultSet.getString("ACCOUNT_STATUS");
			log.info("Account status" + account_status);
		}
		
		
		
	
		assertEquals("exist", subscriptionUserStatus);
		assertEquals(null, Expire_Date);
		Long purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
		
		resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
		
		
		//suspend
		account.remove("status");
		account.put("status", "Suspend");
		reqobj.remove("purchase");
		
		JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
		
		log.debug("Create Purchases Response: " + jsonRes);
		assertNotNull(jsonRes);
		
		//create
		account.remove("status");
		account.put("status", "disconnect");
		//reqobj.remove("purchase");
		
		suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
		
		log.debug("Create Purchases Response: " + jsonRes);
		assertNotNull(jsonRes);
		
		
		log.info("verifying record in account table.");
		resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
		 account_id_guid = "";
		 account_id="";
		 account_status="";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id_guid = resultSet.getString("ACCOUNT_GUID");
			log.info("ACCOUNT_GUID=>" + account_id_guid);
			account_id=resultSet.getString("ACCOUNT_ID");
		
			account_status = resultSet.getString("ACCOUNT_STATUS");
			
		}
		
		log.info("Account Status After Disabled call" + account_status);
		log.info("Verifying Account Status");
		assertEquals("disabled", account_status);
		
	}else{
		log.info("Carrier.fips.code.config is not equal to default");
		fail();
	}
			}
	catch (Exception e) {
		log.info(e.getMessage());
		fail();
	}
			
			}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update account_status as suspend, verify response returns 200 and account_status = suspended, and account table is updated with new value  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5807506
	 */
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_003  (){
		try{
			String extid = "";
			log.info("Starting test case 1st");
			generatePurchaseRequest();
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
			
				account.put("zip_code", "84084");
				
				ResultSet resultSet = null;
		
		JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
		
		
		int i = 0;
		String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

		log.info("verifying record in subscription table.");
		resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
		String subscription_user_id = "";
		String Expire_Date = "";
		if (resultSet.next()) {
			log.info("Fetching Subscription_User_ID from record");
			subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
			Expire_Date = resultSet.getString("EXPIRES_DATE");
			log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
		}

		log.info("verifying record in subscription user table.");
		resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
		String account_id = "";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id = resultSet.getString("ACCOUNT_ID");
			log.info("ACCOUNT_ID=>" + account_id);
		}
		log.info("verifying record in account table.");
		resultSet = commonTableValidators.getDataFromAccountTable(account_id);
		String account_id_guid = "";
		 account_id="";
		 String account_status="";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id_guid = resultSet.getString("ACCOUNT_GUID");
			log.info("ACCOUNT_GUID=>" + account_id_guid);
			account_id=resultSet.getString("ACCOUNT_ID");
		
			account_status = resultSet.getString("ACCOUNT_STATUS");
			log.info("Account status" + account_status);
		}
		
		
		
	
		assertEquals("exist", subscriptionUserStatus);
		assertEquals(null, Expire_Date);
		Long purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
		
		resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
		
		
		//suspend
		account.remove("status");
		account.put("status", "Suspend");
		reqobj.remove("purchase");
		
		JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
		
		log.debug("Create Purchases Response: " + jsonRes);
		assertNotNull(jsonRes);
		
		//create
		account.remove("status");
		account.put("status", "disconnect");
		reqobj.remove("purchase");
		
		suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
		
		log.debug("Create Purchases Response: " + jsonRes);
		assertNotNull(jsonRes);
		
		
		log.info("verifying record in account table.");
		resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
		 account_id_guid = "";
		 account_id="";
		 account_status="";
		if (resultSet.next()) {
			log.info("Fetching ACCOUNT_ID from record");
			account_id_guid = resultSet.getString("ACCOUNT_GUID");
			log.info("ACCOUNT_GUID=>" + account_id_guid);
			account_id=resultSet.getString("ACCOUNT_ID");
		
			account_status = resultSet.getString("ACCOUNT_STATUS");
			
		}
		
		log.info("Account Status After Disabled call" + account_status);
		log.info("Verifying Account Status");
		assertEquals("disabled", account_status);
		
	}else{
		log.info("Carrier.fips.code.config is not equal to default");
		fail();
	}
			}
	catch (Exception e) {
		log.info(e.getMessage());
		fail();
	}
		log.info("ending test case 3rd....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'account_status' from disconnect to suspend, verify resp. returns 200 & account_status as suspended in acct. table is upd. with new value
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5807516
	 *
	 *Following test case is ignored as the Bussiness logic is changed.
	 */
	@Ignore
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_004  (){
		
		String extid = "";
		log.info("Starting test case 4th");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", "84084");
			
			ResultSet resultSet = null;
			reqobj.remove("purchase");
		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);

			}


			log.info("Calling without purchase object and with Disconnect Status");
			{
				account.put("status", "disconnect");
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("disabled", status);
			}
			
			
			log.info("Calling without purchase object and with Suspend Status");
			{
				account.put("status", "suspend");
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("suspended", status);
			}
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 4th....");
		}
		
		log.info("ending test case 4th....");
		
	}
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If	If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'account_status' from disconnect to null , verify response returns 200 and account_status remains unchanged 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5814700
	 *Following test case is ignored as the Bussiness logic is changed.
	 */
	@Ignore
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_005  (){
		
		String extid = "";
		log.info("Starting test case 5th");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", "84084");
			
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);

			}


			log.info("Calling without purchase object and with Disconnect Status");
			{
				account.put("status", "disconnect");
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("disabled", status);
			}
			
			
			log.info("Calling without purchase object and with NULL Status");
			{
				String status = null;
				account.put("status", status);
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				 status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("disabled", status);
			}
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 5th....");
		}
		
		log.info("ending test case 5th....");
		
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'account_status' from suspend to null, verify response returns 200 and account_status remains unchanged 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=external_system_zip
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5814710
	 */
	@Test
	public void SSC_Generic_Billing_Apater_UpdatePartnerAccount_AccountStatus_006  (){
		
		String extid = "";
		log.info("Starting test case 6th");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", "84084");
			
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
		
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);

			}


			log.info("Calling without purchase object and with Suspend Status");
			{
				account.put("status", "suspend");
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("suspended", status);
			}
			
			
			log.info("Calling without purchase object and with NULL Status");
			{
				String status = null;
				account.put("status", status);
				reqobj.remove("purchase");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

				log.info("" + conn.getResponseCode());
				
				log.info("checking the response code");
				assertEquals(200, conn.getResponseCode());
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String res = "";
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {

					System.out.println(output);
					res += output;
				}

				jsonRes = new JSONObject(res);

				conn.disconnect();

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				 status= "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
					status  =resultSet.getString("ACCOUNT_STATUS");
					log.info("Status => " + status);
				}
				
				log.info("checking the status of account");
				assertEquals("enabled", status);
			}
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 6th....");
		}
		
		log.info("ending test case 6th....");
		
	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	
}
