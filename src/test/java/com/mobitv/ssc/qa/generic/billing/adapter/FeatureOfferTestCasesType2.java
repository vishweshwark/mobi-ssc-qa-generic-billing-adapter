package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mortbay.log.Log;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;

import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;


//Carrier=cspire, product=omnia ,partner=cspire
/**
*
* @author 
*
* @test.suite.description
* Feature Offer 
*
* @test.prerequisite
* Generic Billing Adapter_FeatureOffer_Sprint67
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6656
*/
@Ignore
public class FeatureOfferTestCasesType2 extends MobiTestBase {

	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "4_TV2STRM";
	
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = "T5476848";
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	
	ConfigReaderHelper configreader = new ConfigReaderHelper("F_OfferType2_cspire.properties");

	
	@BeforeClass
	public static void initialConditions(){
	
		genericUtil.uploadToFTP("./src/test/resources/config/F_OfferType2_cspire/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
	}
	
	
	private void generatePurchaseRequest() {
		

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

		}

	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With the Active Account status and quantity configuration in xml file if user sends created request for feature offer which is not existing to that user, creates the purchase. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476854
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_09() {
		log.info("Running test case 9th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				// log.info("Response Status Code==>" + response.getStatus());
				// assertTrue(response.getStatus() == 200);
				// verifying table data

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);

			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 9th....");
}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With an Active Account status and quantity
	 *                   configuration for feature.offer.config, having same
	 *                   feature offer already exists and user sends same offer
	 *                   then existing offer shall not get cancelled and new
	 *                   offer shall not get purchased.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476855
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_10() {
		log.info("Running test case 10th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
		try {
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
	
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
		

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			
		

				ResultSet resultSet = null;
				JSONObject jsonRes;
		
		
				
			//	account.put("status", accStatus);
				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);

				JSONObject purchase1 = null;

				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

					purchase1 = getPurchase();

					purchase1.put("product_id", product_id3);

					extendedpropertyarray = new JSONArray();
					extendedproperty = new JSONObject();

					extendedproperty.put("name", "quantity");
					extendedproperty.put("value", "2");

					extendedpropertyarray.put(extendedproperty);
					purchase1.put("extended_property", extendedpropertyarray);

					purchasearray.put(purchase1);

					System.out.println();
				}

				jsonRes = genericUtil.webResponse(externalid, reqobj);
				assertNotNull(jsonRes);
				{
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
					}
					assertEquals("exist", subscriptionUserStatus);
					assertEquals(null, Expire_Date);

				}

			
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
		}
		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
		
		log.info("ending test case 10th....");
	}
		
	


	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description
	 *  With an Active Account status and qnty conf. for feature.offer.config having same feature offer already existing and user sends same feature type offer but with different qnty then existing offer shall get cancelled and new offer shall get purchased. 
	 *  
	 *  Partner=cspire
	 * region.config=offers
	 * .fips.code.config=partner
	 *  
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476856
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_11() {
		log.info("Running test case 11th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
			log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);

				log.info("creating purchase with diffrent quantity");
				
				addQuantity("name", "3");
				jsonRes = genericUtil.webResponse(external_id, reqobj);
				assertNotNull(jsonRes);
				{
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
					}
					assertEquals("exist", subscriptionUserStatus);
			

				}

			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 11th....");
}

	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
	 *  With an active account status and quantity configuration for feature.offer.config having no feature offer available to user and user sends two same types of feature offer with same quantity then only single feature offer shall get created. 
	 * 
	 * Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.verification
	 *  Verifies the API returns status code 200.
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476857
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_12() {
		log.info("Running test case 12th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
	
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				JSONObject purchase1 = null;

				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

					purchase1 = getPurchase();

					purchase1.put("product_id", product_id2);

					extendedpropertyarray = new JSONArray();
					extendedproperty = new JSONObject();

					extendedproperty.put("name", "quantity");
					extendedproperty.put("value", "2");

					extendedpropertyarray.put(extendedproperty);
					purchase1.put("extended_property", extendedpropertyarray);

					purchasearray.put(purchase1);

					System.out.println();
				}
				
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
				
				
				log.info("" + conn.getResponseCode());
				
				log.info("verifying the response");
				assertEquals(400, conn.getResponseCode());
				

			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 12th....");
}

	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
		With an Active account status and quantity configuration for feature.offer.config, if user sends feature offer with the same quantity than that of the existing(Expired) purchased feature offer creates features offer purchase for user. 	 * 
	 * @test.verification
	 *  Verifies the API returns status code 200.
	 * 
	 * Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476858
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_13() {
		log.info("Running test case 13th ......");

		generatePurchaseRequest();
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
			log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				

				///////// Calling Cancel Action
				log.info("Cancel action");
				purchase.put("action", "cancel");
				purchase.put("expires_date", xml_start_date);

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);

				//////////// Calling Create with same
				
				log.info("Create Purchase Response: " + jsonRes);
				purchase.put("action", "create");
				purchase.put("start_date", xml_start_date);
				
				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				{
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
					}
					assertEquals("exist", subscriptionUserStatus);
					

				}
			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 13th....");
}
	
	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
		With the suspended account status if user sends create request for feature offer then feature offer shall get created and Account status shall change to enabled. 
	 * @test.verification
	 *  Verifies the API returns status code 200.
	 * 
	* Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476859
	 */
	
	@Test
	public void  GenericBillingAdapter_FeatureOffer_14 (){
		log.info("Running test case 14th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
			log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
			

				///////// Calling Cancel Action
				{
				account.put("status", "suspend");
				log.info("Cancel action");
				purchase.put("action", "cancel");
				purchase.put("purchase_date", "2017-01-08T19:35:00.000-00:00");

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					String accountstatus="";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
						accountstatus= resultSet.getString("ACCOUNT_STATUS");
					}
					assertEquals("exist", subscriptionUserStatus);
					assertEquals("suspended", accountstatus);
					

				}

				//////////// Calling Create with same
				
				log.info("Create Purchase Response: " + jsonRes);
				{
				account.put("status", "");
				purchase.put("action", "create");
				purchase.put("start_date", xml_start_date);
				
				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					String accountstatus="";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
						accountstatus= resultSet.getString("ACCOUNT_STATUS");
					}
					assertEquals("exist", subscriptionUserStatus);
				
					assertEquals("enabled", accountstatus);

				}
			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 14th....");
}
	
	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
		With the "disabled" account status if user sends create request for feature offer then purchase shall get created and account status is set to "enabled". 
	 * @test.verification
	 *  Verifies the API returns status code 200.
	 * 
	* Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476860
	 */
	@Test
	public void  genericBillingAdapter_FeatureOffer_15(){
		log.info("Running test case 15th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");	
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", "TV2JXN");

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				

				///////// Calling Cancel Action
				log.info("Cancel action");
				account.put("status", "disconnect");
				purchase.put("action", "cancel");
				purchase.put("purchase_date", "2017-01-08T19:35:00.000-00:00");

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				{
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					String accountstatus="";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
						accountstatus= resultSet.getString("ACCOUNT_STATUS");
					}
					assertEquals("exist", subscriptionUserStatus);
					assertEquals("disabled", accountstatus);
					

				}

				//////////// Calling Create with same
				
				log.info("Create Purchase Response: " + jsonRes);
				purchase.put("action", "create");
				purchase.put("start_date", xml_start_date);
				
				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Cancel Purchase Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				{
					i = 0;
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					String accountstatus="";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
						accountstatus= resultSet.getString("ACCOUNT_STATUS");
					}
					assertEquals("exist", subscriptionUserStatus);
				
					assertEquals("enabled", accountstatus);

				}
			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 15th....");
}
	
	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
		With an active account status and feature.offer.config = quantity and user sends Cancel request without the quantity parameter, shall cancel the existing sibscription and return HTTP 200. 	 * @test.verification
	 
	 *  Verifies the API returns status code 200.
	 * 
	* Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476850
	 */
	
	
	@Test
	public void  genericBillingAdapter_FeatureOffer_17 (){
		log.info("Running test case 17th ......");

		generatePurchaseRequest();
		log.info("Checking Partner Config");

		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
			log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

			try {

				System.out.println(configreader.getConfigValue(carrier + ".feature.offer.config"));
				purchase.put("product_id", "TV2JXN");

			} catch (JSONException e) {

				log.error(e.getMessage());
			}

		}

		ResultSet resultSet = null;

		try {
				JSONObject purchase1 = getPurchase();
				purchase1.put("product_id", "TV2JXN");
			
			JSONObject jsonRes = genericUtil.webResponse(external_id, reqobj);
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
			}
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}

		log.info("ending test case 17....");
}
	

	
	
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	public JSONObject getPurchaseWithExpiry() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("expires_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	public void addQuantity(String name, String value) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			purchase.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
