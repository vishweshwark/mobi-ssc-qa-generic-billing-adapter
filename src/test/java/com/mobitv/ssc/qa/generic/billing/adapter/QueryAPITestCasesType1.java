package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire
/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_Query API_Sprint67_SSC8096
*
* @test.prerequisite
* Query API
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6698
*/
public class QueryAPITestCasesType1 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	private String partner=config.getProperty("Partner");
	private String Secret = config.getProperty(carrier+"secret");
	private String externalid = genericUtil.getRandomExternalId();
	
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("QueryAPI_Type1.properties");

	@BeforeClass
	public static void initialConditions(){
		genericUtil.uploadToFTP("./src/test/resources/config/QueryAPI_Type1/partner-config");
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
		
	}
	
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			

			account = new JSONObject();
			account.put("status", "");
			account.put("fips_code", "test_fips_code1cc");
			/*account.put("county","Utah");
			account.put("state","UT");*/
			

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws JSONException 
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If a user has an active sub to a single TV Prem service, and Carrier sends us a cancel notification with status = "suspend", verify Query API response returns updated purchase obj including account object with status = "suspend" and FIPS Code 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
		cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549118
	 */
	
	@Test
	public void  genericBillingAdapter_QueryAPI_01 () {
		log.info("Running test case 1st ......");

		generatePurchaseRequest();

		// Checking the Offer Type
				try {
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("default")) {
			try {

				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}

				{
					log.info("Cancelling offer with status suspend");
					account.put("status", "suspend");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
					}
					{

						log.info("Calling for  the Query API Response");

						jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

						log.debug("Create Purchases Response: " + jsonRes);
						assertNotNull(jsonRes);

						String status = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status",
								"account", 0);

						log.info("Verifying the Account Status");
						assertEquals("suspend", status);

					}

				}
			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
				log.info("Ending Test Case 1st");
			}
		} else {
			log.info("Carrier.user.identifier is not equal to Partner");
			log.info("The value fips.code.config : " + configreader.getConfigValue(carrier + ".fips.code.config"));
			log.info("Ending Test Case 1st");

		}
	} 
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * Verify if the Query Purchase API returns 200 with empty list if user has no subscriptions. Should return Account status as well. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549119
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_02(){
		log.info("Running test case 2nd ......");

		generatePurchaseRequest();

		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				
				log.info("Purchase with create action");
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status empty");
					log.info("Cancelling offer with action cancel ");
					
					account.put("status", "");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;					
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
				
				log.info("Verifying the Account Status");
				assertEquals("active", status);
				
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 2nd....");	
				fail(e.getMessage());
			 }
			log.info("ending test case 2nd....");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *	If a user has an active sub to a single TV Prem service, and Carrier sends us a cancel notification with status = "disconnect", verify Query API response returns updated purchase obj including account object with status = "disconnect" and FIPS Code.  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549120
	 */
	
	
	@Test
	public void genericBillingAdapter_QueryAPI_03(){
		log.info("Running test case 3rd ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
				
		ResultSet resultSet = null;
		JSONObject jsonRes;
	
			try {
				
				log.info("Purchase with create action");
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status disconnect");
					log.info("Cancelling offer with action cancel ");
					account.put("status", "disconnect");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
			
			log.info("Verifying the Account Status");
			assertEquals("disconnect", status);
			assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 3rd....");
				fail(e.getMessage());
			 }
			log.info("ending test case 3rd....");
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *	 If a user has an active sub to a single TV Prem service, and Carrier sends us a cancel notification with status = empty value, verify Query API response returns updated purchase obj including account object with status = "active" and FIPS Code. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	  * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549121
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_04(){
		log.info("Running test case 4th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				
				log.info("Purchase with create action");
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status empty");
					log.info("Cancelling offer with action cancel");
					account.put("status", "");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
			
			log.info("Verifying the Account Status");
			assertEquals("active", status);
			assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 4th....");
				fail(e.getMessage());
			 }	
			log.info("ending test case 4th....");
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If a user has an active sub to a single TV Prem service, and Carrier sends us a cancel notification with status = null, verify Query API response returns updated purchase obj including account object with status = "active" and FIPS Code 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * 
	  * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549122
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_05(){
		log.info("Running test case 5th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				
				log.info("Purchase with create action");
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status null");
					log.info("Cancelling offer with action cancel ");
					String value =null;
					account.put("status",value);
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
				assertEquals("active", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("Running test case 5th ......");
				fail(e.getMessage());
			 }
			log.info("Running test case 5th ......");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If a user has an active subs to offer A and offer B, and Carrier sends us a cancel notification for offer B with status = "suspend", verify Query API response returns updated purchase obj including account object with status = "suspend" and FIPS Code 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549123
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_06(){
		log.info("Running test case 6th ......");

		generatePurchaseRequest();
		
		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create action");
				JSONObject purchase1 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2STOR");
					purchase.put("product_id", "TV2JXN");
				}else{
					purchase1.put("product_id",	"4_TV2STRM");
					purchase.put("product_id", "2_TV2STOR");
				}
				
				purchasearray.put(purchase1);
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status suspend");
					log.info("Cancelling offer with action  cancel");
					generatePurchaseRequest();
					String value ="suspend";
					account.put("status",value);
					purchase.put("action", "cancel");
					
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2JXN");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "2_TV2STOR");
						purchase.put("expires_date", xml_start_date);
					}
					
					
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
			log.info("Verifying the Account Status");
			
				assertEquals("suspend", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("Running test case 6th ......");
				fail(e.getMessage());
			 }
			log.info("Running test case 6th ......");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 	If a user has an active subs to offer A and offer B, and Carrier sends a cancel notification for offer B w/ status = "disconnect", verify Query API response returns updated purchase obj including account objt with status = "disconnect" and FIPS Code. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549124
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_07(){
		log.info("Running test case 7th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
				
		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create action");
				JSONObject purchase1 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2STOR");
					purchase.put("product_id", "TV2JXN");
				}else{
					purchase1.put("product_id",	"4_TV2STRM");
					purchase.put("product_id", "2_TV2STOR");
				}
				purchasearray.put(purchase1);
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status disconnect");
					log.info("Cancelling offer with action cancel ");

					generatePurchaseRequest();
					String value ="disconnect";
					account.put("status",value);
					purchase.put("action", "cancel");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2JXN");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "4_TV2STRM");
						purchase.put("expires_date", xml_start_date);
					}
					
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
			
			log.info("Verifying the Account Status");
				assertEquals("disconnect", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("Running test case 7th ......");
				fail(e.getMessage());
			 }
			log.info("Running test case 7th ......");
		}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If a user has an active subs to offer A, Carrier sends a cancel notification w/ status = "suspend", then user subscribes to diff offer, verify Query API response returns updated purchase obj including account obj with status = "active" and FIPS Code. 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549125
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_08(){
		log.info("Running test case 8th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
		ResultSet resultSet = null;
		JSONObject jsonResp;
		
			try {
				
				log.info("Purchase with create action");
				jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling the offer with suspend status");
					String value ="suspend";
					account.put("status",value);
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				/////
				log.info("Creating other purchase with different offer");
				generatePurchaseRequest();
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						try {

							log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
							addQuantity("name", "2");
							purchase.put("product_id", product_id2);

						} catch (Exception e) {
							log.error(e.getMessage());
						}
					}else{
						purchase.put("product_id", "4_TV2STOR");
					}
					
					{
						log.info("Calling for  the Query API Response");

						jsonResp = genericUtil.webResponse(externalid, reqobj);
						 i = 0;
						log.debug("Create 1st Purchases Response: " + jsonResp);
						assertNotNull(jsonResp);
						// assertTrue(response.getStatus() == 200);

						// verifying table data

						{

							String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
							log.info("verifying record in subscription table.");
							resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
							String subscription_user_id = "";
							if (resultSet.next()) {
								log.info("Fetching Subscription_User_ID from record");
								subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
								log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

							}

							log.info("verifying record in subscription user table.");
							resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
							String account_id_guid = "";
							if (resultSet.next()) {
								log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
								account_id_guid = resultSet.getString("ACCOUNT_ID");
								log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

							}
							log.info("verifying record in account table.");
							String account_id = "";
							resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
							if (resultSet.next()) {
								log.info("Fetching ACCOUNT_ID from Account record");
								account_id = resultSet.getString("ACCOUNT_ID");
								log.info("ACCOUNT_ID=>" + account_id);

							}
							assertEquals("exist", subscriptionUserStatus);
						
					}
				{
					log.info("Verifying the JSON response for status and fips");
				jsonResp = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonResp, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonResp, "fips_code", "account", 0);
			
			log.info("Verifying the Account Status");
				assertEquals("active", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("Running test case 8th ......");
				fail(e.getMessage());
			 }
			log.info("Running test case 8th ......");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If a user has an active subs to offer A, Carrier sends a cancel notification w/ status = "disconnect", then user subscribes to diff offer, verify Query API response returns updated purchase obj incl. account obj with status = "active"and FIPS Code  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549126
	 *
	 *Following Testb case ignored as the Business logic is changed
	 */
	@Ignore
	@Test
	public void genericBillingAdapter_QueryAPI_09(){
		log.info("Running test case 9th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}

		ResultSet resultSet = null;
		JSONObject jsonResp;
		
			try {
				
				log.info("Purchase with create action");

				jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling the offer with disconnect status");
					String value ="disconnect";
					account.put("status",value);
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data

					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				/////
				log.info("Creating other purchase with different offer");
				generatePurchaseRequest();
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						try {

							log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
							addQuantity("name", "2");
							purchase.put("product_id", "TV2STOR");

						} catch (Exception e) {
							log.error(e.getMessage());
						}
					}
					
					{
						
						jsonResp = genericUtil.webResponse(externalid, reqobj);
						 i = 0;
						log.debug("Create 1st Purchases Response: " + jsonResp);
						assertNotNull(jsonResp);
						// assertTrue(response.getStatus() == 200);

						// verifying table data

						{

							String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
							log.info("verifying record in subscription table.");
							resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
							String subscription_user_id = "";
							if (resultSet.next()) {
								log.info("Fetching Subscription_User_ID from record");
								subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
								log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

							}

							log.info("verifying record in subscription user table.");
							resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
							String account_id_guid = "";
							if (resultSet.next()) {
								log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
								account_id_guid = resultSet.getString("ACCOUNT_ID");
								log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

							}
							log.info("verifying record in account table.");
							String account_id = "";
							resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
							if (resultSet.next()) {
								log.info("Fetching ACCOUNT_ID from Account record");
								account_id = resultSet.getString("ACCOUNT_ID");
								log.info("ACCOUNT_ID=>" + account_id);

							}
							assertEquals("exist", subscriptionUserStatus);
						
					}
				{
					log.info("Verifying the JSON response for status and fips");
				jsonResp = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonResp, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonResp, "fips_code", "account", 0);
				
			log.info("Verifying the Account Status");
				assertEquals("active", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("Running test case 9th ......");
				fail(e.getMessage());
			 }
			log.info("Running test case 9th ......");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If user has active subs to offer A & B, Carrier sends cancel notification w/ status = "suspend" for offerA and w/ status = "disconnect" for offerB, verify Query API resp returns updated purch obj incl. acct obj with status ="disconnect" and FIPS Code 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549127
	 *Following Testb case ignored as the Business logic is changed
	 */
	@Ignore
	@Test
	public void genericBillingAdapter_QueryAPI_010(){
		log.info("Running test case 10th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
		log.info("Checking offer type");
		try {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		

		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create action");

				JSONObject purchase1 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2STOR");
					purchase.put("product_id", "TV2JXN");
				}else{
					purchase1.put("product_id",	"4_TV2STRM");
					purchase.put("product_id", "2_TV2STOR");
				}
				purchasearray.put(purchase1);
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Purchase with create action");

					generatePurchaseRequest();
					addQuantity("name", "2");
					String value ="suspend";
					account.put("status",value);
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2JXN");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "4_TV2STRM");
						purchase.put("expires_date", xml_start_date);
					}
					
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
					generatePurchaseRequest();
					addQuantity("name", "2");
					value ="disconnect";
					account.put("status",value);
					
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2STOR");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "2_TV2STOR");
						purchase.put("expires_date", xml_start_date);
					}
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create  Purchase Response: " + jsonResp);
					assertNotNull(jsonResp);
					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
				
			log.info("Verifying the Account Status");
				assertEquals("disconnect", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 10th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 10th....");
		}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If user has active subs to offer A & B, Carrier sends cancel notification w/ status = "disconnect" for offerA and w/ status = "suspend" for offerB, verify Query API resp returns updated purch obj incl. acct obj with status ="suspend" and FIPS Code  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * Following tesyt cases ignored as the Business logic is changed.
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549128
	 */
	@Ignore
	@Test
	public void genericBillingAdapter_QueryAPI_011(){
		log.info("Running test case 11th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
		log.info("Checking offer type");
				try {
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}

		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create action");

				JSONObject purchase1 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2JXN");
					purchase.put("product_id", "TV2STOR");
				}else{
					purchase1.put("product_id",	"4_TV2STRM");
					purchase.put("product_id", "2_TV2STOR");
				}
				purchasearray.put(purchase1);
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				{

					String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
					String subscription_user_id = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					String account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
						account_id_guid = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

					}
					log.info("verifying record in account table.");
					String account_id = "";
					resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from Account record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);

					}
					assertEquals("exist", subscriptionUserStatus);
				}
				
				{
					log.info("Cancelling offer with status disconnect");
					log.info("Cancelling offer with action cancel ");
					generatePurchaseRequest();
					addQuantity("name", "2");
					String value ="disconnect";
					account.put("status",value);
					
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2JXN");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "4_TV2STRM");
						purchase.put("expires_date", xml_start_date);
					}
					
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
					
					log.info("Cancelling offer with status disconnect");
					log.info("Cancelling offer with action cancel ");
					generatePurchaseRequest();
					addQuantity("name", "2");
					value ="suspend";
					account.put("status",value);
					
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						purchase.put("product_id", "TV2STOR");
						purchase.put("expires_date", xml_start_date);
						
					}else{
						purchase.put("product_id", "2_TV2STOR");
						purchase.put("expires_date", xml_start_date);
					}
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					{

						String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
						log.info("verifying record in subscription table.");
						resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
						String subscription_user_id = "";
						if (resultSet.next()) {
							log.info("Fetching Subscription_User_ID from record");
							subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
							log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

						}

						log.info("verifying record in subscription user table.");
						resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
						String account_id_guid = "";
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
							account_id_guid = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

						}
						log.info("verifying record in account table.");
						String account_id = "";
						resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
						if (resultSet.next()) {
							log.info("Fetching ACCOUNT_ID from Account record");
							account_id = resultSet.getString("ACCOUNT_ID");
							log.info("ACCOUNT_ID=>" + account_id);

						}
						assertEquals("exist", subscriptionUserStatus);
				}
				
				
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
			String status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status","account", 0);
			String fips_code = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "fips_code", "account", 0);
			log.info("Verifying the Account Status");
				assertEquals("suspend", status);
				assertNotNull(fips_code);
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 11th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 11th....");
		}
	
	
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If user subscribed, cancelled (suspend), and resubscribed to same offer , verify Query Purchase API w/ valid timestamp and sign returns both Active and Expired subs including extended properties and status in account object as "active".     
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	  * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549132
	 */
	@Test
	public void  genericBillingAdapter_QueryAPI_015(){
		log.info("Running test case 15th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
	
		JSONObject jsonRes;
		
			try {
				
				log.info("Purchase with create action");

				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				//Cancelling purchase
				{
					log.info("Cancelling offer with status suspend");
					log.info("Cancelling offer with action cancel ");
					account.put("status", "suspend");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
				}
				
				// Creating purchase
				{
					log.info("Cancelling offer with status empty");
					log.info("Cancelling offer with action create ");
					account.put("status", "");
					purchase.put("action", "create");
					purchase.remove("expires_date");
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
				}
				
				
				{
				
				
				{
				log.info("Calling for  the Query API Response");
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
				log.info("Verifying the Account Status");
				String status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 0);
				assertEquals("active",status);
				
				status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 1);
				assertEquals("expired", status);
				
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 15th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 15th....");
		}
	 
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If user subscribed, cancelled (disconnect), and resubscribed to same offer , verify Query Purch API w/ valid timestamp and sign returns both Active and Expires subs including extended properties and status in account object as "active".      
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549133
	 *Following Testb case ignored as the Business logic is changed
	 */
	@Ignore
	@Test
	public void  genericBillingAdapter_QueryAPI_016(){
		log.info("Running test case 16th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
				
		
		JSONObject jsonRes;
		
			try {
		
				log.info("Purchase with create action");

				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create  Purchase Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				//Cancelling purchase
				{
					log.info("Cancelling offer with status disconnect");
					log.info("Cancelling offer with action cancel ");
					account.put("status", "disconnect");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
				}
				
				// Creating purchase
				{
					log.info("Cancelling offer with status empty ");
					log.info("Cancelling offer with action create ");
					account.put("status", "");
					purchase.put("action", "create");
					purchase.remove("expires_date");
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
				}
				
				
				{
				
				
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
				log.info("Verifying the Account Status");
				String status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 0);
				assertEquals("active",status);
				
				status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 1);
				assertEquals("expired", status);
				
				status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status", "account", 0);
				assertEquals("active", status);
				   
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 16th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 16th....");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 		If user subscribed, cancelled (suspend), and resubscribed to different offer, verify Query Purch API w/ valid timestamp and sign returns both Active and Expires subs including extended properties and status in account object as "active".      
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	* cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549134
	 */
	@Test
	public void  genericBillingAdapter_QueryAPI_017(){
		log.info("Running test case 17th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				
				
				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}
			
			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");
				
			}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region","TESTREGION");
				account.put("county","Utah");
				account.put("state","UT");
			}

			// Checking the region config
			
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		
		JSONObject jsonRes;
		
			try {

				log.info("Purchase with create action");

				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				
				{
					log.info("Cancelling offer with status suspend");
					log.info("Cancelling offer with action cancel ");
					account.put("status", "suspend");
					purchase.put("action", "cancel");
					purchase.put("expires_date", xml_start_date);
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create  Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
				}
				
				// Creating purchase
				{
					log.info("Cancelling offer with status empty");
					log.info("Cancelling offer with action create");
					account.put("status", "");
					purchase.put("action", "create");
					purchase.remove("expires_date");
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create  Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
				}
				
				
				{
				
				
				{
				log.info("Calling for  the Query API Response");
				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug(" Query API Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				log.info("Verifying the Account Status");
				String status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 0);
				assertEquals("active",status);
				
				status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 1);
				assertEquals("expired", status);
				
				 status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status", "account", 0);
				 assertEquals("active", status);
				   
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 17th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 17th....");
		}
	 
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  Verify if query API return HTTP error code 401 when signature id is mismatched.       
	 * 
	 * @test.verification Verifies the API returns status code 401.
	 * 
	 * 
	* cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549135
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_018(){
		log.info("Running test case 18th ......");

		generatePurchaseRequest();
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					}
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}


	

			try {
				
				log.info("Request With incorrect partner");
				String epochTime = GenericUtil.generateTimestamp();// Get from request URL
				String signature=genericUtil.generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
				HttpURLConnection conn = genericUtil.webMissingParameterResponse("T5476848", null,partner,signature+"test",epochTime+"122345");

				log.info("" + conn.getResponseCode());
				
				log.info("Verifying the response Code");
				assertEquals(401, conn.getResponseCode());
				

				conn.disconnect();
				
			}
		
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 18th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 18th....");
		}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * Verify if the Query Purchase API returns 200 with empty list if user has no subscriptions. Should return Account status as well.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2104086
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_19(){
		log.info("Running test case 19th ......");

		generatePurchaseRequest();

		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");

				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}

			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");

			} else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region", "TESTREGION");
				account.put("county", "Utah");
				account.put("state", "UT");
			}

			// Checking the region config

		} catch (Exception e) {
			log.error(e.getMessage());
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;

		try {

			log.info("Purchase with create action");
			reqobj.remove("purchase");
			JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
			int i = 0;
			log.debug("Create 1st Purchases Response: " + jsonResp);
			assertNotNull(jsonResp);
			

			log.info("Calling for  the Query API Response");
			jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

			assertNotNull(jsonRes);

			String status = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status", "account", 0);

			log.info("Verifying the Account Status");
			assertEquals("active", status);

		} catch (Exception e) {
			log.error(e.getMessage());
			log.info("ending test case 19th....");
			fail(e.getMessage());
		}
		log.info("ending test case 19th....");
	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
    * 
	* @test.description 
	  Verify Query API response returns HTTP 400 when invalid status is passed in the Query API call.  	
	  fips.code.config=default
	  region.config=default

			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151078
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_022(){
		
		log.info("Starting test case 22");
		generatePurchaseRequest();
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			
		try {
			JSONObject jsonRes;
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");

			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
			}
			
			log.info("inserting invalid account status");
			account.put("status", "ABCD");
			
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			

		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 22nd....");
		}
		}else{
		log.info("Carrier.fips.code.config is not equal to default");
		}
		log.info("ending test case 22nd....");
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  Verify Query API response returns equipment_ID for the user if has equipment ID in the PARTNER_ACCOUNT table
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * cspire.fips.code.config=default
		cspire.fips.code.default=49047
		cspire.region.config=offers
		cspire.feature.offer.config=quantity
		
		directlink.user.identifiers=directlink_user_id:external_id,equipment_id:account_extended_property
		directlink.fips.code.config=external_system_zip
		directlink.region.config=default
		directlink.feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2409937
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_23(){
		log.info("Running test case 23th ......");

		generatePurchaseRequest();

		// Checking the Offer Type
		try {
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");

				if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
					purchase.put("product_id", "TV2JXN");
				} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
					// ADD Object value
					account.remove("fips_code");
					account.put("fips_code", "test_fips_code1cc");
					account.put("region", "TESTREGION");

				} else {
					log.info("region.code.config=default");
				}

			}

			log.info("Checking Partner Config");

			if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
				account.remove("fips_code");
				account.put("zip_code", "84036");

			} else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
				account.put("region", "TESTREGION");
				account.put("county", "Utah");
				account.put("state", "UT");
			}

			// Checking the region config

		} catch (Exception e) {
			log.error(e.getMessage());
		}

		ResultSet resultSet = null;
		JSONObject jsonRes;

		try {
			
			
			log.info("Purchase with create action");
			
			extendedproperty = new JSONObject();
			extendedpropertyarray = new JSONArray();
			
			extendedproperty.put("name","equipment_id");
			extendedproperty.put("value","test123");
			extendedpropertyarray.put(extendedproperty);
			
			account.put("extended_property", extendedpropertyarray);
		
			JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
			int i = 0;
			log.debug("Create 1st Purchases Response: " + jsonResp);
			assertNotNull(jsonResp);
			

			log.info("Calling for  the Query API Response");
			jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

			assertNotNull(jsonRes);

			String status = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status", "account", 0);
			JSONArray str =(JSONArray)  genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "extended_property", "account", 0);
			log.info("Verifying the Account Status");
			assertEquals("active", status);

		} catch (Exception e) {
			log.error(e.getMessage());
			log.info("ending test case 23th....");
			fail(e.getMessage());
		}
		log.info("ending test case 23th....");
	}
	
	
	public void addQuantity(String name, String value) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			purchase.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
			
		}
		
	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	
	
	
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
}
