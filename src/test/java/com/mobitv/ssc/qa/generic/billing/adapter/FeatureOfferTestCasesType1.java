package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mortbay.log.Log;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv, product=paytv , partner=ezv

/**
*
* @author 
*
* @test.suite.description
* Feature Offer 
*
* @test.prerequisite
* Generic Billing Adapter_FeatureOffer_Sprint67
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6656
*/
public class FeatureOfferTestCasesType1 extends MobiTestBase {

	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "4_TV2STRM";
	private final String product_id1 = "1_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";
	
	//Server Credentials
    private String uploadPath  = "/opt/mobi-ssc-generic-billing-adapter/conf";
    private String userName = config.getProperty("server.username");
    private String password = config.getProperty("server.password");
    private String server = config.getProperty("QA_PLAT_SVCAR");
	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = "T5476848";
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	
	ConfigReaderHelper configreader = new ConfigReaderHelper("F_OfferType1_directlink.properties");
	
	
	@BeforeClass
	public static void initialConditions(){
	
		genericUtil.uploadToFTP("./src/test/resources/config/F_OfferType1_directlink/partner-config");
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
		}
	
	
	
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

		}

	}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With the Active Account status and exact value configuration in xml file if user sends created request for feature offer which is not existing to that user, creates the purchase.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * .fips.code.config=partner
	 * 
	 * @test.reference 	https://testrail.mobitv.corp/index.php?/tests/view/5476846
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_01() {
		log.info("Running test case 1st ......");
		
		
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		// System.out.println(configreader.getConfigValue(carrier+".feature.offer.config"));
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			

			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);
				
			} catch (Exception e) {
				log.error(e.getMessage());
			}

		}

		ResultSet resultSet = null;

		try {
			
			JSONObject jsonRes = genericUtil.webResponse(external_id, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
			}
			log.info("verifying the Purchase");
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("ending test case 1st....");
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With an Active Account status having same feature offer already existing and user sends same DVR type offer then existing offer shall not get cancelled and new offer shall not get purchased. 
	 * 
	 * Partner=directlink
	 * region.config=default
	 * fips.code.config=partner
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476847
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_02() {
		log.info("Running test case 2nd ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		ResultSet resultSet = null;

		try {

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
			}
			log.info("verifying the Purchase");
			// 2nd purchase request
			JSONObject jsonRes2 = genericUtil.webResponse(externalid, reqobj);
			assertEquals("exist", subscriptionUserStatus);
			

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("ending test case 2nd....");

	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With an Active Account status having same feature offer already existing and user sends different DVR type offer then existing offer shall get cancelled and new offer shall get purchased. 
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=partner
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476848
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_03() {
		log.info("Running test case 3rd ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		ResultSet resultSet = null;

		try {

			JSONObject jsonResp = genericUtil.webResponse(external_id, reqobj);
			int i = 0;
			log.debug("Create 1st Purchases Response: " + jsonResp);
			assertNotNull(jsonResp);
			// assertTrue(response.getStatus() == 200);

			// verifying table data

			{

				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
				String subscription_user_id = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);

				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID_GUID from Subscription User record");
					account_id_guid = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID_GUID=>" + account_id_guid);

				}
				log.info("verifying record in account table.");
				String account_id = "";
				resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from Account record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);

				}
				assertEquals("exist", subscriptionUserStatus);
			}

			

			purchase.put("product_id", product_id1);
			jsonResp = genericUtil.webResponse(external_id, reqobj);

			log.debug("Create 2nd Purchases Response: " + jsonResp);

			assertNotNull(jsonResp);
			// assertTrue(response.getStatus() == 200);

			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonResp, i);
			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonResp, i);
			assertEquals("exist", subscriptionUserStatus);
			String subscription_user_id = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}
			resultSet = commonTableValidators.getExpireDateFromSubscriptionTable(subscription_user_id);
			log.info("Fetching Expire_Date from record");
			while (resultSet.next()) {
				log.info("EXPIRES_DATE==>" + resultSet.getString("EXPIRES_DATE"));
			}

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("ending test case 3rd....");

	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  
	 * With an active account status having no feature offer available to user and user sends two same types of feature offer then only user shall get HTTP 400 and non subscription shall get created. 
	 * 
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=partner
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476849
	 */

	@Test
	public void genericBillingAdapter_FeatureOffer_04() {
		log.info("Running test case 4th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		ResultSet resultSet = null;

		try {

			purchasearray.put(getPurchase());
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			log.info("verifying response");
			assertEquals(conn.getResponseCode(), 400);
			
			

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("ending test case 4th....");
	}

	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	public JSONObject getPurchaseWithExpiry() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("expires_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
	
	
	/**
	 * @throws 
	 * Exception
	 * 
	 * @test.type
	 *  functional
	 * 
	 * @test.description
		With an active account status having no feature offer available to user and user sends two same types of feature offer with Create and Cancel then feature offer shall not get created. Should return 200
 	 * @test.verification
	 
	 *  Verifies the API returns status code 200.
	 * 
	* Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference
	 *  https://testrail.mobitv.corp/index.php?/tests/view/5476850
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_05() {
		log.info("Running test case 5th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		ResultSet resultSet = null;
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create  and Cancel action");
				
				JSONObject purchase1 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2STOR");
					purchase.put("product_id", "TV2STOR");
				}else{
					purchase1.put("product_id",	"4_TV2STOR");
					purchase.put("product_id", "4_TV2STOR");
				}
				purchase1.put("action", "cancel");
				purchasearray.put(purchase1);
				
				String accStatus=null;
				account.put("status", accStatus);
				JSONObject jsonResp = genericUtil.webResponse(external_id, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("Ending test case 5th ......");
	}
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 	With an Active account status if user sends feature offer with the same quantity than that of the existing(Expired/Cancelled) purchased feature offer creates features offer purchase for user. 
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=partner
	 * 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476851
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_06() {
		log.info("Running test case 6th ......");

		generatePurchaseRequest();
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		ResultSet resultSet = null;

		try {

			JSONObject purchase1 = getPurchase();
			purchase1.putOpt("product_id", "2_TV2STOR");
			purchasearray.put(purchase1);
			JSONObject jsonRes = genericUtil.webResponse(external_id, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
			}
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);

			//////// Cancel Subs
			Log.info("calling with action as Cancel with Expiry dates");

			purchase.put("action", "cancel");
			purchase.remove("start_date");
			purchase.putOpt("expires_date", xml_start_date);

			purchase1.putOpt("action", "cancel");
			purchase1.remove("start_date");
			purchase1.put("expires_date", xml_start_date);
			jsonRes = genericUtil.webResponse(external_id, reqobj);

			assertNotNull(jsonRes);

			log.info("calling with action as create");

			purchase.put("action", "create");
			purchase.remove("expires_date");
			purchase.put("start_date", xml_start_date);

			purchase1.put("action", "create");
			purchase1.remove("expires_date");
			purchase1.put("start_date", xml_start_date);

			jsonRes = genericUtil.webResponse(external_id, reqobj);
			assertNotNull(jsonRes);
			{
				subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				subscription_user_id = "";
				Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);
			}

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}
		log.info("Ending test case 6th ......");
	}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  With the suspended account status if user sends create request for feature offer then feature offer shall get created and Account status shall change to enabled. 
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=partner
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476852
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_07() {
		log.info("Running test case 7th ......");

		generatePurchaseRequest();
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		ResultSet resultSet = null;

		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				JSONObject jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);

				// Cancel and status=suspended

				log.info("sending request with action=cancel and account status=suspend");
				purchase.put("action", "cancel");
				account.put("status", "suspend");

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				// sending create request
				{
				log.info("Sending create request with empty account status");
				String accStatus=null;
				purchase.put("action", "create");
				
				account.put("status", accStatus);

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				{
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
					}
					log.info("Verifying records");
					assertEquals("exist", subscriptionUserStatus);
					
				}
				}
			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 7th....");
}

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description
	 * With the "disabled" account status if user sends create request for feature offer then purchase shall get created and account status is set to "enabled" 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * cspire.fips.code.config=partner
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5476853
	 *
	 *Folowing test case has ignore as the requirement is changed.
	 */ 
	@Ignore
	@Test
	public void genericBillingAdapter_FeatureOffer_08() {
		log.info("Running test case 8th ......");

		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}

		}
		ResultSet resultSet = null;

		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("partner")) {
			try {

				JSONObject jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}

				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id = resultSet.getString("ACCOUNT_ID");
					log.info("ACCOUNT_ID=>" + account_id);
				}
				log.info("verifying record in account table.");
				resultSet = commonTableValidators.getDataFromAccountTable(account_id);
				String account_id_guid = "";
				if (resultSet.next()) {
					log.info("Fetching ACCOUNT_ID from record");
					account_id_guid = resultSet.getString("ACCOUNT_GUID");
					log.info("ACCOUNT_GUID=>" + account_id_guid);
				}
				assertEquals("exist", subscriptionUserStatus);
				assertEquals(null, Expire_Date);

				// Cancel and status=disconnect

				log.info("sending request with action=cancel and account status=disconnect");
				account.put("status", "disconnect");
				purchase.put("action", "cancel");

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);

				// sending create request

				log.info("Sending create request with empty account status");
				String accStatus=null;
				purchase.put("action", "create");
				account.put("status",accStatus);

				jsonRes = genericUtil.webResponse(external_id, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				{
					subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

					log.info("verifying record in subscription table.");
					resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
					subscription_user_id = "";
					Expire_Date = "";
					if (resultSet.next()) {
						log.info("Fetching Subscription_User_ID from record");
						subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
						Expire_Date = resultSet.getString("EXPIRES_DATE");
						log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
					}

					log.info("verifying record in subscription user table.");
					resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
					account_id = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id = resultSet.getString("ACCOUNT_ID");
						log.info("ACCOUNT_ID=>" + account_id);
					}
					log.info("verifying record in account table.");
					resultSet = commonTableValidators.getDataFromAccountTable(account_id);
					account_id_guid = "";
					String accountstatus = "";
					if (resultSet.next()) {
						log.info("Fetching ACCOUNT_ID from record");
						account_id_guid = resultSet.getString("ACCOUNT_GUID");
						log.info("ACCOUNT_GUID=>" + account_id_guid);
						accountstatus = resultSet.getString("ACCOUNT_STATUS");
					}
					assertEquals("exist", subscriptionUserStatus);
					assertEquals("enabled", accountstatus);
				}

			} catch (Exception e) {
				log.error(e.getMessage());

				fail(e.getMessage());
			}
		}else{
			
			log.info("Carrier.fips.code.config in not equal to Partner");
			fail();
		}
	}
		log.info("ending test case 8th....");
}


	
		
	
	/**
	* @throws Exception
	 * 
	* @test.type functional
    * 
	* @test.description 
		With an active account status and feature.offer.config = quantity/exact_value and user sends invalid status, then purchase should not be created and should receive HTTP 400.  
	* 
	* 
	* region.config=default
	  .fips.code.config=partner
			
	* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151077
	 */
	@Test
	public void genericBillingAdapter_FeatureOffer_019(){
		
		log.info("Starting test case 19");
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		
		try {
			
			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				purchase.put("product_id", "TV2JXN");
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("fips_code", "test_fips_code1cc");
				account.put("region", "TESTREGION");

			} else {
				log.info("region.code.config=default");
			}
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					log.info("Adding the extended property for Quantity type");

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "0");
					purchase.put("product_id", product_id2);
	
			}
			
			log.info("Inserting Invalid Status");
			purchase.put("status","ABCD");			
			purchase.put("action", "");
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			

		} catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
		}
	}else{
		
		log.info("Carrier.fips.code.config in not equal to Partner");
		fail();
	}

		log.info("ending test case 19th....");
	}
	

	public void addQuantity(String name, String value) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			purchase.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
}
