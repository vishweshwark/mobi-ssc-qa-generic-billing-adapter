package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;

public class RegisterProfileApiTest2 extends MobiTestBase {
	
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");
	

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("SupportActiveStatus.properties");


	  @BeforeClass
	  public static void initialConditions(){
	  
	 genericUtil.uploadToFTP(
	 "./src/test/resources/config/SupportActiveAccount/partner-config");
	 
	  
	 try { Thread.sleep(60000); } catch (InterruptedException e1) { 
	  e1.printStackTrace(); } 
	 }
	 

	private void generatePurchaseRequest() {

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();

			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description   With the existing user, if the user sends request for suspended the account along with various partner accounts, verify response should return 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506747
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_007() {
		try {
			log.info("Starting test case 7");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/07/partner-config");
			 
			  
			Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call without Fips Code");
			account.put("status", "active");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			
			log.info("Suspend call");
			
			account.remove("status");
			account.put("status", "suspend");
			
			reqobj.remove("purchase");
			
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			log.info("Ending test case 7");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 7");
			fail();
		}
	}
	
	//pass
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description   With suspended account, if user request for reactivating the account with account status as active, verify response should return 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506748
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_008() {
		try {
			log.info("Starting test case 8");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/08/partner-config");
			 
			  
			Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call without Fips Code");
			account.put("status", "active");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			
			log.info("Suspend call");
			
			account.remove("status");
			account.put("status", "suspend");
			
			reqobj.remove("purchase");
			
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			log.info("Re-Activation call");
			
			account.remove("status");
			account.put("status", "active");
			
			reqobj.remove("purchase");
			
			
			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			log.info("Ending test case 8");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 8");
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description   With cancelled purchases & suspended acct., if user reactivates the account with acct. status as null, verify response should return 200.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506749
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_009() {
		try {
			log.info("Starting test case 9");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/9/partner-config");
			 
			  
			Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call without Fips Code");
			account.put("status", "active");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			
			log.info("Suspend call");
			
			account.remove("status");
			account.put("status", "suspend");
			
			reqobj.remove("purchase");
			
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			log.info("Re-Activation call");
			
			String status=null;
			account.remove("status");
			account.put("status", status);
			
			//reqobj.remove("purchase");
			
			
			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			log.info("Ending test case 9");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 9");
			fail();
		}
	}
	//test case assertion changed from 400 to 200 as per new requirements change
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With disconnected account, if user request for reactivating the account with account status as active, verify response should return account cannot be created once disconnected.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6506750
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_Register_Profile_API_010() {
		try {
			log.info("Starting test case 9");
	
			/*genericUtil.uploadToFTP(
			 "./src/test/resources/config/RegisterProfileAPI/10/partner-config");
			 
			  
			Thread.sleep(60000); */
			generatePurchaseRequest();

			ResultSet resultSet = null;
			
			log.info(" Create Purchase call without Fips Code");
			account.put("status", "active");
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}

			log.info("Create Purchase call");
			JSONObject jsonRes = genericUtil.webUserResponse(externalid, reqobj, "nova", null);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			String accountStatus = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				accountStatus = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account Status=>" + accountStatus);
			}
			log.info("verifying the Purchase");
			assertEquals("enabled", accountStatus);
			assertEquals("exist", subscriptionUserStatus);
		
			
			log.info("Suspend call");
			
			account.remove("status");
			account.put("status", "disconnect");
			
			reqobj.remove("purchase");
			
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			log.info("Re-Activation call");
			
			String status=null;
			account.remove("status");
			account.put("status", status);
			
			//reqobj.remove("purchase");
			
			
			conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			log.info("Ending test case 9");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("Ending test case 9");
			fail();
		}
	}
	
	
	public void addEquipmentId(String name,String value,JSONObject objectname){
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			account.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}

}
