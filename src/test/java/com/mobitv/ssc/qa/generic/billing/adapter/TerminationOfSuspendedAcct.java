package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;

public class TerminationOfSuspendedAcct extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;
	
	JSONArray emptyPurchaseArray = new JSONArray();
	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("ReActAcctSpicificPackage.properties");

	
	@BeforeClass 
	  public static void initialConditions(){
	 
	  genericUtil.uploadToFTP("./src/test/resources/config/TerminateSuspendedAcct/partner-config");
	  
	  
	  try {
		  Thread.sleep(60000); 
	  }
	  catch (InterruptedException e1) 
	  { // TODO
	 e1.printStackTrace();
	 }
	  }
	 
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	private void generatePurchaseRequest() {
		
		
		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();
			
			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  With the suspended account, if the user sends request for the termination(disconnect) of the account, verify response should return 200 and account table should be updated with new value.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137751
	 */
	@Test
	public void SSC_Generic_billing_adapter_account_termination_001(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			 String account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account status" + account_status);
			}
			
			
			
		
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("disabled", account_status);
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with account_status as terminate and if subscriptions are intact, verify response returns 200 and account_status = terminate for suspended account, and account table
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137752
	 */
	@Test
	public void SSC_Generic_billing_adapter_account_termination_002(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			 String account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account status" + account_status);
			}
			
			
			
		
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//create
			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");
			
			JSONObject	suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("disabled", account_status);
			
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description When suspending all the purchases along with account disconnect then when reactivating create all the same packages along with add packages, it should return 200 and account table should be updated.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708614
	 */
	@Test
	public void SSC_Generic_billing_adapter_account_termination_003(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			 String account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account status" + account_status);
			}
			
			
			
		
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//suspend
			log.info("API call with suspend status and cancel action");
			account.remove("status");
			account.put("status", "suspend");
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			JSONObject	suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("suspended", account_status);
			
			//suspend
			log.info("API call with null status and create action");
			account.remove("status");
			String status=null;
			account.put("status",status);
			purchase.remove("action");
			purchase.put("action", "create");
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("enabled", account_status);
			
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description   With the disconnected account status if user sends reactivation request without account status change, verify response returns 400
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2455231
	 */
	@Test
	public void SSC_Generic_billing_adapter_account_termination_005(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			 String account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account status" + account_status);
			}
			
			
			
		
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//disconnect
			account.remove("status");
			account.put("status", "disconnect");
			reqobj.remove("purchase");
			
			JSONObject	suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("disabled", account_status);
			
			
			//React
			log.info("API call with null status and create action");
		
			
			purchase.remove("action");
			purchase.put("action", "create");
			purchasearray.put(purchase);
			reqobj.put("purchase", purchasearray);
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info("" + conn.getResponseCode());

			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  When suspending the account purchases are still active then disconnecting one of the cancelled existing purchases and create one additional purchase/offer, it should return account status as 200 and account table gets updated.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6708615
	 */
	@Test
	public void SSC_Generic_billing_adapter_account_termination_004(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			 String account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				log.info("Account status" + account_status);
			}
			
			
			
		
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			//disconnect
			log.info("API call with suspend status and cancel action");
			account.remove("status");
			account.put("status", "disconnect");
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			JSONObject	suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("disabled", account_status);
			
			//React
			log.info("API call with null status and create action");
			account.remove("status");
			String status=null;
			account.put("status",status);
			purchase.remove("action");
			purchase.put("action", "create");
			
			suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			 account_id_guid = "";
			 account_id="";
			 account_status="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			
				account_status = resultSet.getString("ACCOUNT_STATUS");
				
			}
			
			log.info("Account Status After Disabled call" + account_status);
			log.info("Verifying Account Status");
			assertEquals("enabled", account_status);
			
			
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
}
