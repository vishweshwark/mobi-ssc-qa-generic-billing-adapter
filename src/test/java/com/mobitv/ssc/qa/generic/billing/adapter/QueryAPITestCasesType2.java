package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire
/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_Query API_Sprint67_SSC8096
*
* @test.prerequisite
* Query API
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6698
*/
public class QueryAPITestCasesType2 extends MobiTestBase{
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	private String partner=config.getProperty("Partner");
	private String Secret = config.getProperty(carrier+"secret");
	private String externalid = genericUtil.getRandomExternalId();
	
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("QueryAPI_Type2.properties");
	
	
	@BeforeClass
	public static void initialConditions(){
		genericUtil.uploadToFTP("./src/test/resources/config/QueryAPI_Type2/partner-config");
				try {
					Thread.sleep(180000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
		
	}
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			

			account = new JSONObject();
			account.put("status", "");
			account.put("fips_code", "test_fips_code1cc");
			/*account.put("county","Utah");
			account.put("state","UT");*/
			

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * 	If a user has an active sub to a single TV Prem service, verify Query API call with 'partner' missing in the request returns 404.  
	 * 
	 * @test.verification Verifies the API returns status code 404.
	 * 
	 * 
	 * region.config=offers
	 * .fips.code.config=partner
	 * feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549129
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_012(){
		log.info("Running test case 12th ......");

		generatePurchaseRequest();
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				log.info("Adding the extended property for Quantity type");

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("default")) {
			try {
				log.info("Verifying Resonse Code");

				String epochTime = GenericUtil.generateTimestamp();// Get from request URL
				String signature=genericUtil.generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
				HttpURLConnection conn = genericUtil.webMissingParameterResponse(externalid, null,"",signature, epochTime);

				log.info("" + conn.getResponseCode());
				assertEquals(404, conn.getResponseCode());
				

				conn.disconnect();
			
			}
		
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 12th....");
				fail(e.getMessage());
			 }
		}else{
			log.info("Partner Config not equal to default....");
		}
		log.info("ending test case 12th....");
	}
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If a user has an active sub to a single TV Prem service, verify Query API call with 'signature' missing in the request returns 400.   
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * region.config=offers
	 * .fips.code.config=partner
	 * feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549130
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_013(){
		log.info("Running test case 13th ......");

		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				log.info("Adding the extended property for Quantity type");

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("default")) {
			try {
				log.info("Verifying Resonse Code");
				String epochTime = genericUtil.generateTimestamp();// Get from request URL
				String signature=genericUtil.generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
				HttpURLConnection conn = genericUtil.webMissingParameterResponse(externalid, null,partner,"", epochTime);

				log.info("" + conn.getResponseCode());
				assertEquals(400, conn.getResponseCode());
				

				conn.disconnect();
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				
				
			
			}
		
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 13th....");
				fail(e.getMessage());
			 }
		}
		log.info("ending test case 13th....");
	}
	

	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If a user has an active sub to a single TV Prem service, verify Query API call with 'ts' missing in the request returns 400.    
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * region.config=offers
	 * .fips.code.config=default
	 * feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549131
	 */
	@Test
	public void genericBillingAdapter_QueryAPI_014(){
		log.info("Running test case 14th ......");

		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				log.info("Adding the extended property for Quantity type");
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier + ".fips.code.config").equals("default")) {
			try {
				log.info("Verifying Response Code");
				String epochTime = GenericUtil.generateTimestamp();// Get from request URL
				String signature=genericUtil.generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
				HttpURLConnection conn = genericUtil.webMissingParameterResponse(externalid, null,partner,signature,"");

				log.info("" + conn.getResponseCode());
				assertEquals(400, conn.getResponseCode());
				

				conn.disconnect();
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				
				
			
			}
		
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 14th....");
				fail(e.getMessage());
			 }
		}
		log.info("ending test case 14th....");
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *   Verify Query Purchase API with valid timestamp and signature returns all Active, Scheduled, Expired and Canceled Subscriptions including extended properties and proper status in account object.        
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * region.config=offers
	 * .fips.code.config=partner
	 * feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2104087
	 */
	@Test
	public void  genericBillingAdapter_QueryAPI_020(){
		log.info("Running test case 20th ......");

		generatePurchaseRequest();
		
		// Checking the Offer Type
				try {
					log.info("Checking offer type");
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {

						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2");
						
						}
						if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
							purchase.put("product_id", "TV2JXN");
						} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
							// ADD Object value
							account.remove("fips_code");
							account.put("fips_code", "test_fips_code1cc");
							account.put("region", "TESTREGION");

						} else {
							log.info("region.code.config=default");
						}

					
					
					log.info("Checking Partner Config");

					if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_system_zip")) {
						account.remove("fips_code");
						account.put("zip_code", "84036");
						
					}else if (configreader.getConfigValue(carrier + ".fips.code.config").equals("external_county_state")) {
						account.put("region","TESTREGION");
						account.put("county","Utah");
						account.put("state","UT");
					}

					// Checking the region config
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}

		
		JSONObject jsonRes;
		
			try {
				log.info("Purchase with create action");

				JSONObject purchase1 = getPurchase();
				JSONObject purchase2 = getPurchase();
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					purchase1.put("product_id",	"TV2STOR");
					
				}else{
					purchase1.put("product_id",	"2_TV2STOR");
					
				}
				
				
				purchasearray.put(purchase1);
			
				JSONObject jsonResp = genericUtil.webResponse(externalid, reqobj);
				int i = 0;
				log.debug("Create 1st Purchases Response: " + jsonResp);
				assertNotNull(jsonResp);
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				//Cancelling purchase
				{
					log.info("Cancelling offer with status empty");
					log.info("Cancelling offer with action cancel");
					
					if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
						
						purchase1.put("product_id", "TV2STOR");
						
					}else{
						
						purchase1.put("product_id",	"2_TV2STOR");
						
					}
					account.put("status", "");
					purchase.put("action", "cancel");
					
					
				
					purchase.put("expires_date", xml_start_date);
					
					
					jsonResp = genericUtil.webResponse(externalid, reqobj);
					 i = 0;
					log.debug("Create 1st Purchases Response: " + jsonResp);
					assertNotNull(jsonResp);
					// assertTrue(response.getStatus() == 200);

					// verifying table data
				}
				{
		
				{
				log.info("Calling for  the Query API Response");

				jsonRes = genericUtil.webQueryAPIResponse(externalid, reqobj);

				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
					
				log.info("Verifying the Account Status");
				String status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 0);
				assertEquals("active",status);
				
				status	 = (String) genericUtil.getDataFromJSONArrayResponseForQueryAPI(jsonRes, "status", "purchase", 1);
				assertEquals("expired", status);
				   
				   
				 status	 = (String) genericUtil.getDataFromJSONObjectResponseQueryAPI(jsonRes, "status", "account", 0);
				 assertEquals("active", status);
				   
				}
			
			}
		}
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 20th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 20th....");
		}
	 
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  Verify query API response returns 401 unauthorized when partner parameter passed is incorrect.  
	 * 
	 * @test.verification Verifies the API returns status code 401.
	 * 
	 * Partner=cspire
	 * region.config=offers
	 * cspire.fips.code.config=partner
	 * feature.offer.config=exact_value
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5549138
	 */
	
	@Test
	public void genericBillingAdapter_QueryAPI_021(){
		log.info("Running test case 20th ......");

		generatePurchaseRequest();
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {

				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2");
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}

			try {
			
				String epochTime = GenericUtil.generateTimestamp();// Get from request URL
				String signature=genericUtil.generateSignature("partner/v1/core/purchase/"+carrier+"/"+externalid+"/purchases",carrier,epochTime);
				HttpURLConnection conn = genericUtil.webMissingParameterResponse(externalid, null,"directlink+cbc",signature,epochTime);

				log.info("" + conn.getResponseCode());
				assertEquals(401, conn.getResponseCode());
				

				conn.disconnect();
				// assertTrue(response.getStatus() == 200);

				// verifying table data

				
				
			
			}
		
			 catch (Exception e) {
				log.error(e.getMessage());
				log.info("ending test case 21th....");
				fail(e.getMessage());
			 }
			log.info("ending test case 21th....");
		}
	
	
	public void addQuantity(String name, String value) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			purchase.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
			log.info("ending test case 22nd....");
		}
		log.info("ending test case 22th....");
	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
}
