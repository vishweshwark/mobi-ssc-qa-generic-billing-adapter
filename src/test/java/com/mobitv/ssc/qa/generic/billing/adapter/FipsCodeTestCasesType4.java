package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;
import com.sun.jersey.api.client.ClientResponse;

//Carrier=ezv , product=paytv , partner= ezv
//Carrier=cspire , product=omnia , partner=cspire

/**
*
* @author 
*
* @test.suite.description
* fipscode Offer 
*
* @test.prerequisite
* Process_FIPS_Code
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6693
*/
public class FipsCodeTestCasesType4 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "2_TV2STRM";
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	
		ConfigReaderHelper configreader = new ConfigReaderHelper("Fips_Code_Type4.properties");

		@BeforeClass
		public static void initialConditions(){
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/partner-config");
			
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
		}
		
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
			public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);

				
			}
			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}

	/**
	* @throws Exception
	 * 
	* @test.type functional
	* 
	* @test.description 
		With the fips.code.config=partner user does not send any fips_code during create purchase API call, then fips_code shall not be added to partner_account table as well as no purchase will be created.. 	* 
	* @test.verification Verifies the API returns status code 200.
	* 
	* fips.code.config=partner
	* region.config=default
	
			
	* @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544761
	 */
	@Test
	public void Directlink_SSC_GenericBillingAdapter_FipsCode_016(){
		
		log.info("Starting test case 16");
		
		genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/16/partner-config");
		
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		
		generatePurchaseRequest();
		
		log.info("Checking Partner Config");
	
		try {
		
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
			
			ResultSet resultSet = null;
			
			log.info("Checking offer type");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					log.info("Adding the extended property for Quantity type");
	
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					addQuantity("name", "2",purchase);
					purchase.put("product_id", product_id2);
	
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}
			
			log.info("Calling to create an Purchase without fips code");
		//	account.put("fips_code","49013");
			//JSONObject purchase1 = getPurchase();
			
			
			/*purchase1.put("product_id", product_id5);
			purchasearray.put(purchase1);
		*/
			HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

			log.info(""+conn.getResponseCode());
			log.info("Verifying the Response");
			assertEquals(400,conn.getResponseCode());
		
			
			
		
	}
		}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 16th....");
		}
		
		log.info("ending test case 16th....");
		
	}
	
	
	
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
			Create one purchase for user with the configuration as fips.code.config=partner set fips code provided by partner in the account object of API call. 	 
		* @test.verification Verifies the API returns status code 200.
		* 
		* fips.code.config=partner
		* region.config=default
		
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/1928387
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_05(){
			
			log.info("Starting test case 5");
			
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/05/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without fips code");
				account.put("fips_code","49013");
			
				JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
		
		
				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
		
				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}
		
				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			
			
			log.info("Verifying the fips code");
			assertNotNull(fips);
		
			
			
		
	}
		}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 5th....");
		}
		
		log.info("ending test case 5th....");
		
	}
		
		
		
		
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
			With the Active Account status and exact value configuration in xml file if user sends create request for feature offer which is not existing to that user, creates the purchase.
		* @test.verification Verifies the API returns status code 200.
		* 
		* fips.code.config=partner
		* region.config=default
		
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/1928382
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_01(){
			
			log.info("Starting test case 5");
			
			/*genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/05/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/	
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without fips code");
				account.put("fips_code","49013");
			
				JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
				
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
		
		
				int i = 0;
				String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);
		
				log.info("verifying record in subscription table.");
				resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
				String subscription_user_id = "";
				String Expire_Date = "";
				if (resultSet.next()) {
					log.info("Fetching Subscription_User_ID from record");
					subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
					Expire_Date = resultSet.getString("EXPIRES_DATE");
					log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
				}
		
				log.info("verifying record in subscription user table.");
				resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
				String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			String partnername="";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
			String fips = "";
			if (resultSet.next()) {
				partnername = resultSet.getString("PARTNER_NAME");
				fips = resultSet.getString("PARTNER_ACCOUNT_ID");
			}
			
			
			log.info("Verifying the fips code");
			assertNotNull(fips);
		
			
			
		
	}
		}catch (Exception e) {
			log.error(e.getMessage());
	
			fail(e.getMessage());
			log.info("ending test case 5th....");
		}
		
		log.info("ending test case 5th....");
		
	}
	
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		With the fips.code.config=partner/external_system_zip/external_system_county_state/default if user not send Action in incoming API, not create the purchase and return HTTP 400.		* @test.verification Verifies the API returns status code 200.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151074
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_017(){
			
			log.info("Starting test case 17");
			
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/16/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without fips code");
				account.put("fips_code","49013");
				//JSONObject purchase1 = getPurchase();
				
				
				/*purchase1.put("product_id", product_id5);
				purchasearray.put(purchase1);
			*/
				purchase.remove("action");
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(400,conn.getResponseCode());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 17th....");
			}
			
			log.info("ending test case 17th....");
			
		}
	
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		* With the fips.code.config=partner/external_system_zip/external_system_county_state/default if user sending invalid purchase status in incoming API, not create the purchase and return HTTP 400.		
		
		* @test.verification Verifies the API returns status code 400.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151075
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_018(){
			
			log.info("Starting test case 18");
			
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/18/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase with invalid status code");
			
				account.remove("status");
				account.put("status", "activa");
				
				
				
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(400,conn.getResponseCode());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 18th....");
			}
			
			log.info("ending test case 18th....");
			
		}
		
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		*  During create purchase do not add ts parameter in the query param, purchase should not be created and user should get HTTP 400.		
		
		* @test.verification Verifies the API returns status code 400.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151091
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_021(){
			
			log.info("Starting test case 21");
			
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without Account Object");
			
				
				
				ClientResponse conn = genericUtil.webClientResponseMissingTimeStamp(external_id, reqobj);

				log.info(""+conn.getStatus());
				log.info("Verifying the Response");
				assertEquals(400,conn.getStatus());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 21th....");
			}
			
			log.info("ending test case 21th....");
			
		}
		
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		* When account object is missing in the incoming request then the purchase should be created and user gets HTTP 400.		
		
		* @test.verification Verifies the API returns status code 400.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2151080
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_020(){
			
			log.info("Starting test case 20");
			
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without Account Object");
			
				reqobj.remove("account");
				
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(400,conn.getResponseCode());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 20th....");
			}
			
			log.info("ending test case 20th....");
			
		}
		
	
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		* Verify if valid response payload is received when user pass only the Account status object.		
		
		* @test.verification Verifies the API returns status code 400.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2409938
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_022(){
			
			log.info("Starting test case 22");
			
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/22/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase without Account Object");
			
				
				
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(200,conn.getResponseCode());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 22....");
			}
			
			log.info("ending test case 22....");
			
		}
		
		
		
		/**
		* @throws Exception
		 * 
		* @test.type functional
		* 
		* @test.description 
		* Verify if valid response payload is received when only updated the equipment ID through account status.		
		
		* @test.verification Verifies the API returns status code 200.
		* 
		* fips.code.config=partner
		* region.config=default
		
				
		* @test.reference https://testrail.mobitv.corp/index.php?/cases/view/2409939
		 */
		@Test
		public void Directlink_SSC_GenericBillingAdapter_FipsCode_023(){
			
			log.info("Starting test case 23");
			
			genericUtil.uploadToFTP("./src/test/resources/config/Fips_Code_Type4/22/partner-config");
			
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			generatePurchaseRequest();
			
			log.info("Checking Partner Config");
		
			try {
			
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
				
				ResultSet resultSet = null;
				
				log.info("Checking offer type");
				if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
					try {
						log.info("Adding the extended property for Quantity type");
		
						log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
						addQuantity("name", "2",purchase);
						purchase.put("product_id", product_id2);
		
					} catch (Exception e) {
						log.error(e.getMessage());
					}
				}
				
				log.info("Calling to create an Purchase With Account Object");
			
				
				addEquipmentId("equipment_id", "beforemodify", extendedproperty);
				
				HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(200,conn.getResponseCode());
				
				//remove purchase and add equipment_id
				generatePurchaseRequest();
				reqobj.remove("purchase");
				
				addEquipmentId("equipment_id", "modified", extendedproperty);
				
				conn = genericUtil.webStatusResponse(external_id, reqobj);

				log.info(""+conn.getResponseCode());
				log.info("Verifying the Response");
				assertEquals(200,conn.getResponseCode());
			
				
				
			
		}
			}catch (Exception e) {
				log.error(e.getMessage());
		
				fail(e.getMessage());
				log.info("ending test case 23....");
			}
			
			log.info("ending test case 23....");
			
		}
		
		
		public void addEquipmentId(String name,String value,JSONObject objectname){
			extendedpropertyarray = new JSONArray();
			extendedproperty = new JSONObject();

			try {
				extendedproperty.put("name", name);
				extendedproperty.put("value", value);

				extendedpropertyarray.put(extendedproperty);
				account.put("extended_property", extendedpropertyarray);

			} catch (JSONException e) {
				log.info(e.getMessage());
			}

		}
}

