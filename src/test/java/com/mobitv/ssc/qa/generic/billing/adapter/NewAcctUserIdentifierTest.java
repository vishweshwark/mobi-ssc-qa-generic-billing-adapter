package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mobitv.MobiTestBase;

public class NewAcctUserIdentifierTest extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("SupportActiveStatus.properties");

	@BeforeClass
	public static void initialConditions() {

		genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	private void generatePurchaseRequest() {

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();
			account.put("fips_code", "49013");
			account.put("zip_code", "84007");
			account.put("county", "Duchesne");
			account.put("state", "UT");
			account.put("region", "meridian");

			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114135
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_001() {
		try {
			log.info("Starting test case 1");

			generatePurchaseRequest();

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			
			log.info("Create Purchase call");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response : " + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());

			
			log.info("Ending test case 1");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}

	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=true and user sending the User Identifier in the purchase request then account should be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114136
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_002() {
		try {
			log.info("Starting test case 2");

			generatePurchaseRequest();

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Adding Equipment_id");
			
			addEquipmentProperty("equipment_id", "test1234");
			
			log.info("Create Purchase call");

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			
			assertNotNull(jsonRes);
			
			log.info("Ending test case 2");
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=false and user not sending the User Identifier in the purchase request then account should be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114137
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_003() {
		try {
			log.info("Starting test case 3");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/UserIdentifierFalse/partner-config");
			generatePurchaseRequest();

			account.put("status", "");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
		
			log.info("Adding Equipment_id");
			addEquipmentProperty("equipment_id", "test1234");
			
			log.info("Create Purchase call");
		
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			
			assertNotNull(jsonRes);
			
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
			log.info("Ending test case 3");
	
		} catch (Exception e) {
			log.info(e.getMessage());
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=false and user not sending the User Identifier in the purchase request then account should be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114138
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_004() {
		try {
			log.info("Starting test case 4");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/UserIdentifierFalse/partner-config");
			generatePurchaseRequest();

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Adding Equipment_id");
			
			addEquipmentProperty("equipment_id", "test1234");
			
			log.info("Create Purchase call");

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
	
			log.debug("Create Purchases Response: " + jsonRes);
			
			assertNotNull(jsonRes);
			
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
			log.info("Ending test case 4");
	
		} catch (Exception e) {
			log.info(e.getMessage());
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true and user sending the User Identifier as blank ("user_identifier":"") in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114139
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_005() {
		try {
			log.info("Starting test case 5");
			
			generatePurchaseRequest();

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Adding Equipment_id");
			
			addEquipmentProperty("equipment_id", " ");
		
			log.info("Create Purchase call");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response : " + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
		
			log.info("Ending test case 5");
	
		} catch (Exception e) {
			log.info(e.getMessage());
			
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=false and user not sending the User Identifier in the purchase request then account should be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114140
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_006() {
		try {
			log.info("Starting test case 6");
			
			generatePurchaseRequest();

			account.put("status", "active");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
		
			log.info("Adding Equipment_id");
			addEquipmentProperty("equipment_id", "test1234");
			
			log.info("Create Purchase call with existing user");
			HttpURLConnection conn = genericUtil.webStatusResponse("26621054", reqobj);
			
			
			log.info("Response " + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			log.info("Ending test case 6");
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true and user not sending the all User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114141
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_007() {
		try {
			log.info("Starting test case 7");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/ExternalSystemZip/partner-config");
			generatePurchaseRequest();
			
			account.put("status", "");
			account.remove("zip_code");

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
		
			addEquipmentProperty("equipment_id", "retest_Id123");
			log.info("Create Purchase call");

			log.info("Create Purchase call with existing user");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 7");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=<dummy_value> and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114142
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_008() {
		try {
			log.info("Starting test case 8");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/UserIdentifierEmpty/partner-config");
			generatePurchaseRequest();

			account.put("status", "");
			

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
		
			log.info("Create Purchase call ");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 8");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description :For a new user provisioning with partnername.user.indentifiers.mandatory=<dummy_value> and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114143
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_009() {
		try {
			log.info("Starting test case 9");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/UserIdentifierWithDummyValue/partner-config");
			generatePurchaseRequest();

			account.put("status", "");
			

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Create Purchase call ");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 9");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}

	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true, carrier.user.identifiers=<dummy_id>:account_extended_propertry and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7114144
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_010() {
		try {
			log.info("Starting test case 10");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/CarrierUserIdentifierDummy/partner-config");
			generatePurchaseRequest();
		
			account.put("status", "");
			

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Create Purchase call ");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 10");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true, carrier.user.identifiers=<dummy_id>:account_extended_propertry and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7161001
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_011() {
		try {
			log.info("Starting test case 11");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/CarrierUserIdentifierDummy/partner-config");
			generatePurchaseRequest();

			account.put("status", "");
			

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
			
			log.info("Create Purchase call ");
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(400, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 11");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description : For a new user provisioning with partnername.user.indentifiers.mandatory=true, carrier.user.identifiers=<dummy_id>:account_extended_propertry and user not sending the User Identifier in the purchase request then account should not be provisioned.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/tests/view/7161011
	 */
	@Test
	public void SSC_GenericBA_UserIdentifier_012() {
		try {
			log.info("Starting test case 12");
			
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
			generatePurchaseRequest();

			account.put("status", "");
			

			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {

					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));

					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			} else {
				purchase.put("product_id", "2_TV2STOR");
			}
	
			log.info("Adding Equipment_id");
			addEquipmentProperty("equipment_id", "test1234");
			

			log.info("Create Purchase call");

			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("Response :" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(200, conn.getResponseCode());
			
			
			
	
		} catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}finally {
			log.info("Ending test case 12");
			genericUtil.uploadToFTP("./src/test/resources/config/NewAcctUserIdentifier/partner-config");
		}
	}
	
	
	public void addEquipmentProperty(String propertyname,String propertyvalue) throws JSONException{
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();
		
		extendedproperty.put("name", propertyname);
		extendedproperty.put("value", propertyvalue);
		
		extendedpropertyarray.put(extendedproperty);
		
		account.put("extended_property",extendedpropertyarray);
	}
}
