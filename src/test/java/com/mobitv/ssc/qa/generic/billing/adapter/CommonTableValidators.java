package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.util.JDBCHelper;

public class CommonTableValidators extends MobiTestBase {
	GenericUtil genericUtil = new GenericUtil();

	// Database configs
	// PM
	private final String purchMgrDb = "purchase_managers";
	// AM
	private final String acctMgrDb = "ACCTMGMT";
	// RM
	private final String rightsDb = "rights_manager";

	// method to verify whether record exist in subscription_user table or not
	
	public String verifySubscriptionUserTable(JSONObject jsonResponse, int i) {
		Connection connection = null;
		ResultSet resultSet = null;
		String status = null;
		String subscriptionUserId = null;

		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		try {
			connection = genericUtil.getDbConnection(purchMgrDb);
			resultSet = genericUtil.tableOperation(connection, "SUBSCRIPTION_USER_ID", "SUBSCRIPTION",
					"SUBSCRIPTION_ID", subID + "");
			if (resultSet != null) {
				while (resultSet.next()) {
					subscriptionUserId = resultSet.getString("SUBSCRIPTION_USER_ID");
				}
				status = genericUtil.checkEntryExistInTable(connection,  "SUBSCRIPTION_USER", "SUBSCRIPTION_USER_ID", subscriptionUserId + "");
			}
		} catch (SQLException e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				log.error(e.toString());
			}
		}
		return status;
	}

	// method to verify whether record exist in promotion_used table or not
	public String verifyPromotionUsedTable(JSONObject jsonResponse, int i) {
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "PROMOTION_USED", "SUBSCRIPTION_ID", subID + "");		
	}

	// method to get data from subscription table
	public ResultSet getDataFromSubscriptionTable(JSONObject jsonResponse, int i) {
		Connection connection = null;
		ResultSet resultSet = null;

		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);

		try {
			connection = genericUtil.getDbConnection(purchMgrDb);
			resultSet = genericUtil.tableOperation(connection, "*", "SUBSCRIPTION", "SUBSCRIPTION_ID", subID + "");
		} catch (Exception e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		}
		return resultSet;
	}
	
	//method to return expire date
	public ResultSet getExpireDateFromSubscriptionTable(String SUBSCRIPTION_USER_ID) {
		Connection connection = null;
		ResultSet resultSet = null;
		try {
			connection = genericUtil.getDbConnection(purchMgrDb);
			resultSet = genericUtil.tableOperation(connection, "*", "SUBSCRIPTION", "SUBSCRIPTION_USER_ID", SUBSCRIPTION_USER_ID + "");
		} catch (Exception e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		}
		return resultSet;
	}
	
	// method to get data from subscription user table
		public ResultSet getDataFromSubscriptionUserTable(String subscription_user_id) {
			Connection connection = null;
			ResultSet resultSet = null;
			try {
				connection = genericUtil.getDbConnection(purchMgrDb);
				resultSet = genericUtil.tableOperation(connection, "*", "SUBSCRIPTION_USER", "SUBSCRIPTION_USER_ID", subscription_user_id + "");
			} catch (Exception e) {
				log.error("testDb: DB error " + e);
				fail(e.getMessage());
			}
			return resultSet;
		}

		// method to get data from account table
				public ResultSet getDataFromAccountTable(String account_guid) {
				
					Connection connection = null;
					ResultSet resultSet = null;
					try {
						connection = genericUtil.getDbConnection(acctMgrDb);
						resultSet = genericUtil.tableOperation(connection, "*", "ACCOUNT", "ACCOUNT_GUID", account_guid + "");
					} catch (Exception e) {
						log.error("testDb: DB error " + e);
						fail(e.getMessage());
					}
					return resultSet;
				}

	// method to verify whether record exist in billing_history table or not
	public String verifyBillingHistoryTable(JSONObject jsonResponse, int i) {
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);   
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "BILLING_HISTORY", "SUBSCRIPTION_ID", subID + "");
	}
	
	public ResultSet getBillingHistoryTable(JSONObject jsonResponse, int i,String subID) {
		ResultSet resultSet;
		//Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);   
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
	resultSet = genericUtil.getBillingHistory(connection,  "BILLING_HISTORY", "SUBSCRIPTION_ID", subID + "");
	
	return resultSet;
	}
	
	//method to verify the RIGHTS table in rights_manager db
	public String verifyRightsTable(String accountId) {
		Connection connection = genericUtil.getDbConnection(rightsDb);
		return genericUtil.checkEntryExistInTable(connection,  "RIGHTS", "ACCOUNT_ID", accountId);
	}
	
	//method to verify whether record exist in action_log table or not
	public String verifyActionLogTable(JSONObject jsonResponse, int i) {		
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "ACTION_LOG", "SUBSCRIPTION_ID", subID + "");		
	}
	
	// method to verify whether record exist in subscription table or not
	public String verifySubscriptionTable(JSONObject jsonResponse, int i) {
		
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "SUBSCRIPTION", "SUBSCRIPTION_ID", subID + "");		
	}
		
	// method to verify whether pending_transaction in promotion_used table or not
	public String verifyPendingTransactionTable(JSONObject jsonResponse, int i) {
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "pending_transaction", "SUBSCRIPTION_ID", subID + "");
	}
		
	// method to verify whether pending_transaction_archive in promotion_used table or not
	public String verifyPendingTransactionArchiveTable(JSONObject jsonResponse, int i) {
		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		Connection connection = genericUtil.getDbConnection(purchMgrDb);
		return genericUtil.checkEntryExistInTable(connection,  "pending_transaction_archive", "SUBSCRIPTION_ID", subID + "");		
	}
	
	// method to verify whether pending_transaction_archive in promotion_used table or not
	public String verifyNotificationLogTable(JSONObject jsonResponse, int i) {
		Connection connection = null;
		ResultSet resultSet = null;
		String notification_log_status = null;
		String subscriptionUserId = null;

		Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
		log.debug("Purchases ID: " + subID);
		try {
			connection = genericUtil.getDbConnection(purchMgrDb);
			resultSet = genericUtil.tableOperation(connection, "SUBSCRIPTION_USER_ID", "SUBSCRIPTION",
					"SUBSCRIPTION_ID", subID + "");
			if (resultSet != null) {
				while (resultSet.next()) {
					subscriptionUserId = resultSet.getString("SUBSCRIPTION_USER_ID");
				}
				notification_log_status = genericUtil.checkEntryExistInTable(connection,  "NOTIFICATION_LOG", "SUBSCRIPTION_USER_ID", subscriptionUserId + "");
			}
		} catch (SQLException e) {
			log.error("testDb: DB error " + e);
			fail(e.getMessage());
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				log.error(e.toString());
			}
		}
		return notification_log_status;		
	}
	
	// method to verify whether record exist in account table in ACCNTMNGR or not
		public String getDataFromAccountTable(JSONObject jsonResponse, int i) {
			Connection connection = null;
			ResultSet resultSet = null;
			String accountId = null;
			String subscriptionUserId = null;

			Long subID = (Long) genericUtil.getDataFromJSONResponse(jsonResponse, "purchase_id", i);
			log.debug("Purchases ID: " + subID);
			try {
				connection = genericUtil.getDbConnection(purchMgrDb);
				resultSet = genericUtil.tableOperation(connection, "SUBSCRIPTION_USER_ID", "SUBSCRIPTION",
						"SUBSCRIPTION_ID", subID + "");
				if (resultSet != null) {
					while (resultSet.next()) {
						subscriptionUserId = resultSet.getString("SUBSCRIPTION_USER_ID");
					}
				}
				resultSet = null;	
				resultSet = genericUtil.tableOperation(connection, "ACCOUNT_ID", "SUBSCRIPTION_USER",
						"SUBSCRIPTION_USER_ID", subscriptionUserId);
				if (resultSet != null) {
					while (resultSet.next()) {
						accountId = resultSet.getString("ACCOUNT_ID");
					}
				}
			} catch (SQLException e) {
				log.error("testDb: DB error " + e);
				fail(e.getMessage());
			} finally {
				try {
					if (resultSet != null) {
						resultSet.close();
					}
					if (connection != null) {
						connection.close();
					}
				} catch (SQLException e) {
					log.error(e.toString());
				}
			}
			return accountId;		
		}
		
		// method to verify whether entry exist in ACCOUNT table.
		public String verifyAccountTable(String accoutId) {
			Connection connection = genericUtil.getDbConnection(acctMgrDb);
			return genericUtil.checkEntryExistInTable(connection,  "ACCOUNT", "ACCOUNT_GUID", accoutId);		
		}
		
		ResultSet getDataFromRightsTable(String accountId) {
			// SQL_Setup
			Connection connection = null;
			PreparedStatement pstmt = null;
			ResultSet resultSet = null;
			String sql = null;
			
			connection = genericUtil.getDbConnection(rightsDb);
			resultSet = genericUtil.tableOperation(connection, "*", "RIGHTS",
					"ACCOUNT_ID", accountId + "");

			return resultSet;
		}

		//method to match Partner_Account table
		public ResultSet FetchMatchingRecordFromPartnerTable(String account_Id) 
		{
			Connection connection = null;
			ResultSet resultSet = null;
			try
			{
			connection = genericUtil.getDbConnection(acctMgrDb);	
			resultSet = genericUtil.tableOperation(connection, "*", "PARTNER_ACCOUNT","ACCOUNT_ID", account_Id + "");
			}
			catch (Exception e) {
					log.error(e.toString());
				}
		
			return  resultSet;
		}
		
}
