package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv

/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_Update Partner Account_Sprint 70
*
* @test.prerequisite
* Update Partner Account
*
*
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6851
*/
public class UpdatePartnerAccountTest1 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("WithoutNotificatioIndetifier_Type1_directlink.properties");

	@BeforeClass
	public static void initialConditions(){
		genericUtil.uploadToFTP("./src/test/resources/config/test/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
	}
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'equipment_id', verify response returns 200 and equipment_id in partner_account table is updated with new value
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=durectlink
	 * region.config=default
	 * .fips.code.config=default
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5794797
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_UpdatePartnerAcct_001(){
		
		String extid = "";
		String account_id_guid = "";
		String equipmentIdOld="amittest123";
		String equipmentIdNew="amittestABC";
		
		log.info("Starting test case 1st");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
		
			account.put("zip_code", "84084");
			
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
			addEquipmentId("equipment_id",equipmentIdOld, null);
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
				
				
				
				log.info("Calling without purchase object and with  Status");
				
				account.remove("status");
				reqobj.remove("purchase");
				account.remove("extended_property");
				addEquipmentId("equipment_id", equipmentIdNew, null);
				
				

				jsonRes = genericUtil.webResponse(externalid, reqobj);
				extid = externalid;
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				String partnername="";
				String updatedEquipmentId="";
				
				String directlink_user_id = null;
				String fips_code= null;
				String region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				String partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("equipment_id".equalsIgnoreCase(partnername)) {
						updatedEquipmentId = partneraccountid;
						break;
					}
				}
				log.info(" verifying Updated equipment id :" + updatedEquipmentId + " fetched equipmenmt id :" + equipmentIdNew);
				assertEquals(equipmentIdNew,updatedEquipmentId);
			}

			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 1st....");
		}
		
		log.info("ending test case 1st....");
		
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * If partner sends notif. w/ all required params, then subsequent call, notif. rec'd with only acct obj to update 'fips_code' via zip_code, verify response returns 200 and fips_code in partner_account table is updated with new value   
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * .fips.code.config=external_system_zip
	 * change fips.code.config at local also.
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5794798
	 */
	@Ignore
	@Test
	public void SSC_Generic_Billing_Adapter_UpdatePartnerAcct_002(){
		
		String extid = "";
		String account_id_guid = "";
		
		String zipCodeOld="84002";
		String zipCodeNew="97501";
		
		log.info("Starting test case 2");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", zipCodeOld);
			
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
			//addEquipmentId("equipment_id",equipmentIdOld, null);
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);


			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
				
				String partnername="";
				String updatedFipsCode="";
				String oldFips="";
				String directlink_user_id = null;
				String fips_code= null;
				String region = null;
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				String partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("fips_code".equalsIgnoreCase(partnername)) {
						oldFips = partneraccountid;
						break;
					}
				}
				
				log.info("Calling without purchase object and different ZIP");
				
				account.remove("status");
				reqobj.remove("purchase");
				account.remove("zip_code");
				account.put("zip_code", zipCodeNew);
				
				

				jsonRes = genericUtil.webResponse(externalid, reqobj);
				extid = externalid;
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("fips_code".equalsIgnoreCase(partnername)) {
						updatedFipsCode = partneraccountid;
						break;
					}
				}
				log.info("verifying fips code");
				if (oldFips==updatedFipsCode) {
					fail("fips are equal");
				}
				
			}

			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 2nd....");
		}
		
		log.info("ending test case 2nd....");
		
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *  If partner sends notif. w/ all required params, then subs. call, notif. rec'd with only acct obj to update 'region' and default config is set to "DirectLinkRegion", verify resp. returns 200 and region in partner_account table is not updated 
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * .fips.code.config=default
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5794802
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_UpdatePartnerAcct_006(){
		
		
		String extid = "";
		String account_id_guid = "";
		
		String zipCode="84002";
		String fipsCode="49013";
		String county="Duchesne";
		String state="UT";
		String region="meridian";
		
		log.info("Starting test case 2");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", zipCode);
			account.put("fips_code", fipsCode);
			account.put("county",county);
			account.put("state", state);
			account.put("region", region);
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
			//addEquipmentId("equipment_id",equipmentIdOld, null);
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);


			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
				
				String partnername="";
				String updatedRegion="";
				String oldRegion="";
				String newRegion="jackson";
				String directlink_user_id = null;
				String fips_code= null;
				
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				String partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("region".equalsIgnoreCase(partnername)) {
						oldRegion = partneraccountid;
						break;
					}
				}
				
				log.info("Calling without purchase object and different ZIP");
				
				account.remove("status");
				reqobj.remove("purchase");
				account.remove("zip_code");
				account.remove("fips_code");
				account.put("region", newRegion);
				
				

				jsonRes = genericUtil.webResponse(externalid, reqobj);
				extid = externalid;
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("fips_code".equalsIgnoreCase(partnername)) {
						updatedRegion = partneraccountid;
						break;
					}
				}
				log.info("verifying Region code");
				if (oldRegion==updatedRegion) {
					fail("Region not Updated");
				}
				
			}

			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 6th....");
		}
		
		log.info("ending test case 6th....");
		
	}
	
	
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 *   If partner sends notif. w/ all required params, then subs. call, notif. rec'd with only acct obj to update 'region' and default config is set to "partner", verify resp. returns 200 and region in partner_account table is updated  
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=default
	 * .fips.code.config=default
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5816776
	 */
	@Test
	public void SSC_Generic_Billing_Adapter_UpdatePartnerAcct_007  (){
	
		
		String extid = "";
		String account_id_guid = "";
		
		String zipCode="84002";
		String fipsCode="49013";
		String county="Duchesne";
		String state="UT";
		String region="meridian";
		
		log.info("Starting test case 2");
		generatePurchaseRequest();
		
		log.info("Checking offer type");
		if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
			try {
				
				log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
				addQuantity("name", "2",purchase);
				purchase.put("product_id", product_id2);

			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		
		try {
		log.info("Checking Partner Config");

		if (configreader.getConfigValue(carrier+".fips.code.config").equals("external_system_zip")) {
		
			account.put("zip_code", zipCode);
			account.put("fips_code", fipsCode);
			account.put("county",county);
			account.put("state", state);
			account.put("region", region);
			ResultSet resultSet = null;
			//reqobj.remove("purchase");
			//addEquipmentId("equipment_id",equipmentIdOld, null);
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			extid = externalid;
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);


			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			
			 account_id="";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id=resultSet.getString("ACCOUNT_ID");
			}
			
			log.info("verifying record in account table.");
			
			resultSet = commonTableValidators.getDataFromAccountTable(account_id_guid);
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from Account record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
				
				String partnername="";
				String updatedRegion="";
				String oldRegion="";
				String newRegion="jackson";
				String directlink_user_id = null;
				String fips_code= null;
				
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				String partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("region".equalsIgnoreCase(partnername)) {
						oldRegion = partneraccountid;
						break;
					}
				}
				
				log.info("Calling without purchase object and different ZIP");
				
				account.remove("status");
				reqobj.remove("purchase");
				account.remove("zip_code");
				account.remove("fips_code");
				account.put("region", newRegion);
				
				

				jsonRes = genericUtil.webResponse(externalid, reqobj);
				extid = externalid;
				log.debug("Create Purchases Response: " + jsonRes);
				assertNotNull(jsonRes);
				
				
				resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);
				partneraccountid = "";
				while (resultSet.next()) {
					partnername = resultSet.getString("PARTNER_NAME");
					partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
					
					if ("fips_code".equalsIgnoreCase(partnername)) {
						updatedRegion = partneraccountid;
						break;
					}
				}
				log.info("verifying Region code");
				if (oldRegion==updatedRegion) {
					fail("Region not Updated");
				}
				
			}

			
		}else{
			log.info("Carrier.fips.code.config is not equal to external_system_zip");
			
		}
	}catch (Exception e) {
			log.error(e.getMessage());

			fail(e.getMessage());
			log.info("ending test case 7th....");
		}
		
		log.info("ending test case 7th....");
		
	}
	

	
	public void addEquipmentId(String name,String value,JSONObject objectname){
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", name);
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			account.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	
	
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", "2");

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	
}
