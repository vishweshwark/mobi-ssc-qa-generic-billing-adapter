package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.mobitv.MobiTestBase;
import com.mobitv.platform.partner.dto.AccountRequest;
import com.mobitv.platform.partner.dto.PurchaseNotificationBatchRequest;
import com.mobitv.platform.partner.dto.PurchaseRequest;

//Carrier=ezv , product=paytv , partner= ezv

/**
*
* @author 
*
* @test.suite.description
* Sprint67_Generic Billing Adapter_HandleRegionCode_SSC_7781
* @test.reference
* https://testrail.mobitv.corp/index.php?/runs/view/6695
*/
public class RegionCodeTestCaseType2 extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();
	private final String product_id = "2_TV2STRM";
	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";
	private final String product_id3 = "TV2STOR";
	private final String product_id4= "2_TV2STOR";
	private final String product_id5 = "4_TV2STOR";
	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	
	private String carrier = config.getProperty("Carrier");
	
	private String externalid = genericUtil.getRandomExternalId();
	private String external_id = genericUtil.getRandomExternalId();
	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	AccountRequest accountrequest = new AccountRequest();

	PurchaseNotificationBatchRequest batchRequest = new PurchaseNotificationBatchRequest();

	PurchaseRequest request1;
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("RegionCode_Type2_directlink.properties");
	
	@BeforeClass
	public static void initialConditions(){
		genericUtil.uploadToFTP("./src/test/resources/config/RegionCode_Type2_directlink/partner-config");
		
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
	}
	private void generatePurchaseRequest() {
		batchRequest = new PurchaseNotificationBatchRequest();

		request1 = new PurchaseRequest();

		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {
			external_id = genericUtil.getRandomExternalId();

			account = new JSONObject();
			account.put("status", "");
			//account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {
			
			log.info(e.getMessage());
		}

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
 		With an Active Account Status and partnername.region_config=partner user is sending create purchase request with Local Offer creates the purchase and update region in PARTNER_ACCOUNT table sent by Partner in API Call. 	 * 
	 *
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * Partner=directlink
	 * region.config=partner
	 * fips.code.config=partner
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544818
	 */
	
	@Test
	public void genericBillingAdapter_SSC_Region_03(){
		log.info("Starting test case 3rd");
		generatePurchaseRequest();

		ResultSet resultSet = null;
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		try {

			if (configreader.getConfigValue(carrier + ".region.config").equals("offers")) {
				//Region will get extracted from local offers
			} else if (configreader.getConfigValue(carrier + ".region.config").equals("partner")) {
				// ADD Object value
				account.put("region", "TESTREGION");
				account.put("fips_code", "test_fips_code1cc");

			} else {
				// Default value from config
			}

			purchase.put("product_id", "MAX");
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);

			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);

			// log.info("Response Status Code==>" + response.getStatus());
			// assertTrue(response.getStatus() == 200);
			// verifying table data

			int i = 0;
			String subscriptionUserStatus = commonTableValidators.verifySubscriptionUserTable(jsonRes, i);

			log.info("verifying record in subscription table.");
			resultSet = commonTableValidators.getDataFromSubscriptionTable(jsonRes, i);
			String subscription_user_id = "";
			String Expire_Date = "";
			if (resultSet.next()) {
				log.info("Fetching Subscription_User_ID from record");
				subscription_user_id = resultSet.getString("SUBSCRIPTION_USER_ID");
				Expire_Date = resultSet.getString("EXPIRES_DATE");
				log.info("SUBSCRIPTION_USER_ID=>" + subscription_user_id);
			}

			log.info("verifying record in subscription user table.");
			resultSet = commonTableValidators.getDataFromSubscriptionUserTable(subscription_user_id);
			String account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id = resultSet.getString("ACCOUNT_ID");
				log.info("ACCOUNT_ID=>" + account_id);
			}
			log.info("verifying record in account table.");
			resultSet = commonTableValidators.getDataFromAccountTable(account_id);
			String account_id_guid = "";
			account_id = "";
			if (resultSet.next()) {
				log.info("Fetching ACCOUNT_ID from record");
				account_id_guid = resultSet.getString("ACCOUNT_GUID");
				log.info("ACCOUNT_GUID=>" + account_id_guid);
				account_id = resultSet.getString("ACCOUNT_ID");
			}

			String partneraccountid = "";
			resultSet = commonTableValidators.FetchMatchingRecordFromPartnerTable(account_id);

			while (resultSet.next()) {
				partneraccountid = resultSet.getString("PARTNER_ACCOUNT_ID");
				

			}

			assertNotNull(partneraccountid);
			assertEquals("exist", subscriptionUserStatus);
			assertEquals(null, Expire_Date);

		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 3rd....");
		}
	}else{
		log.info("Carrier.fips.code.config is not equal to partner");
		}
		log.info("ending test case 3rd....");
		}
		

	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description 
	 * With an Active Account Status and partnername.region_config=partner having no local offer exists user is sending create purchase request with without any region code in the API call, not creates the region in Partner account table and return 400 
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * Partner=directlink
	 * region.config=partner
	 * .fips.code.config=partner
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/5544819
	 */
	
	
	@Test
	public void genericBillingAdapter_SSC_Region_04(){
		log.info("Starting test case 4th");
		generatePurchaseRequest();
		if (configreader.getConfigValue(carrier+".fips.code.config").equals("partner")) {
		ResultSet resultSet = null;
		
		try {
		JSONObject jsonRes;	
		if (configreader.getConfigValue(carrier+".region.config").equals("offers")) {
			
				}
			else if (configreader.getConfigValue(carrier+".region.config").equals("partner")) {
				//ADD Object value	
				//account.put("fips_code", "test_fips_code1cc");

				}else {
					//ADD object value
				}
			
		
		HttpURLConnection conn = genericUtil.webStatusResponse(external_id, reqobj);

		log.info(""+conn.getResponseCode());
		assertEquals(400,conn.getResponseCode());
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
		
		
		
		String res = "";
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {

			System.out.println(output);
			res+=output;
		}
	
		jsonRes = new JSONObject(res);
		
		conn.disconnect();
		
		log.debug("Create Purchases Response: " + jsonRes);
		assertNotNull(jsonRes);

		} catch (Exception e) {
			log.info(e.getMessage());
			log.info("ending test case 4th....");
		}
	}else{
		log.info("Carrier.fips.code.config is not equal to partner");
		}
		log.info("ending test case 4th....");
		}
	
}
