package com.mobitv.ssc.qa.generic.billing.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mortbay.log.Log;

import com.google.gson.JsonObject;
import com.mobitv.MobiTestBase;

/**
*
* @author 
*
* @test.suite.description
* Generic Billing Adapter_Reactivation of suspended account with specific package changes _Sprint 72
*
* @test.prerequisite
*Reactivation of suspended account with specific package changes
*
* @test.reference
*https://testrail.mobitv.corp/index.php?/runs/view/7000
*/
public class ReActAcctSpecificPackageChange extends MobiTestBase {
	static GenericUtil genericUtil = new GenericUtil();
	CommonTableValidators commonTableValidators = new CommonTableValidators();

	private final String product_id1 = "4_TV2STRM";
	private final String product_id2 = "TV2STRM";

	private final String vendor_purchase_id1 = "646277hsaaasa4z121q1";

	private String carrier = config.getProperty("Carrier");

	private String externalid = genericUtil.getRandomExternalId();

	private final String purchase_origin = "external";
	private final Boolean isTrial = false;

	
	///
	JSONObject reqobj;
	JSONObject purchase;
	JSONObject account;
	JSONObject extendedproperty;

	JSONArray purchasearray;
	JSONArray extendedpropertyarray;

	////
	private GregorianCalendar purchase_date = new GregorianCalendar();
	private GregorianCalendar start_date = new GregorianCalendar();

	XMLGregorianCalendar xml_start_date = null;

	ConfigReaderHelper configreader = new ConfigReaderHelper("ReActAcctSpicificPackage.properties");

	
	@BeforeClass 
	  public static void initialConditions(){
	 
	  genericUtil.uploadToFTP("./src/test/resources/config/ReActAcctSpecificPackage/partner-config");
	  
	  
	  try {
		  Thread.sleep(60000); 
	  }
	  catch (InterruptedException e1) 
	  { // TODO
	 e1.printStackTrace();
	 }
	  }
	 
	public void addQuantity(String name, String value,JSONObject objectname) {
		extendedpropertyarray = new JSONArray();
		extendedproperty = new JSONObject();

		try {
			extendedproperty.put("name", "quantity");
			extendedproperty.put("value", value);

			extendedpropertyarray.put(extendedproperty);
			objectname.put("extended_property", extendedpropertyarray);

		} catch (JSONException e) {
			log.info(e.getMessage());
		}

	}
	private void generatePurchaseRequest() {
		
		
		start_date = new GregorianCalendar();

		try {
			start_date.add(Calendar.DAY_OF_MONTH, (-2));

			xml_start_date = DatatypeFactory.newInstance().newXMLGregorianCalendar(start_date);

		} catch (Exception e) {
			fail();
		}

		try {

			account = new JSONObject();
			
			// account.put("fips_code", "test_fips_code1cc");

			purchase = new JSONObject();
			purchase.put("action", "create");
			purchase.put("product_id", product_id1);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			purchasearray = new JSONArray();
			purchasearray.put(purchase);

			reqobj = new JSONObject();

			reqobj.put("account", account);
			reqobj.put("purchase", purchasearray);
		} catch (Exception e) {

			log.info(e.getMessage());
		}

	}
	
	public JSONObject getPurchase() {

		JSONObject purchase = new JSONObject();

		try {
			purchase.put("action", "create");
			purchase.put("product_id", product_id2);
			purchase.put("vendor_purchase_id", vendor_purchase_id1);
			purchase.put("start_date", xml_start_date);
			purchase.put("purchase_origin", purchase_origin);
			purchase.put("is_trial", isTrial);

			
		} catch (JSONException e) {

			log.error(e.getMessage());
		}
		return purchase;

	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description With the suspended account status if user sends reactivation request with special package changes then in the notification call, the account should get activated with account status changing to active and purchases should be created.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137620
	 */
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_001(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2STRM");
					addQuantity("name", "2", purchase);
					
					
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			account.remove("status");
			account.put("status", "active");
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "MAX");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			reqobj.put("purchase", purchasearray);
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(2, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  With the suspended account status if user sends reactivation request with special package changes then if the subscriptions are intact, verify it returns account status as active
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137621
	 */
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_002(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			//reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + suspendJsonRes);
			assertNotNull(suspendJsonRes);
			
			//create
			account.remove("status");
			account.put("status", "active");
			purchase.remove("action");
			purchase.put("action", "create");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "MAX");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			reqobj.put("purchase", purchasearray);
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History");
			assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  When cancelling all the purchases along with account suspension then when reactivating create all the same packages along with add packages, it should return 200 and account table should be updated.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137622
	 */
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_003(){
		try {
			generatePurchaseRequest();
			
			JSONObject purchase1 = getPurchase();
			JSONObject purchase2 = getPurchase();
			JSONObject purchase3 = getPurchase();
			JSONObject purchase4 = getPurchase();
			
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					//addQuantity("name", "2", purchase);
					
					purchase1.put("product_id", "TV2EXP");
					//addQuantity("name", "4", purchase);
					
					purchase2.put("product_id", "TV2JXN");
					//addQuantity("name", "6", purchase);
					
					purchasearray.put(purchase1);
					purchasearray.put(purchase2);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				purchase2.put("product_id", "HBO");
				
				purchasearray.put(purchase1);
				purchasearray.put(purchase2);
			}
			
			
			
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			purchase1.remove("action");
			purchase1.put("action", "cancel");
			
			purchase2.remove("action");
			purchase2.put("action", "cancel");
			//reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + suspendJsonRes);
			assertNotNull(suspendJsonRes);
			
			//create
			generatePurchaseRequest();
			account.remove("status");
			account.put("status", "active");
			purchase.remove("action");
			purchase.put("action", "create");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					
					purchase.remove("product_id");
					purchase.put("product_id", "TV2COL");
					purchase4.put("product_id", "TV2EXP");
					
					purchasearray.put(purchase);
					purchasearray.put(purchase4);

			}else{
				
				
				purchase.remove("product_id");
				purchase.put("product_id", "MAX");
				purchase4.put("product_id", "Express");
				
				purchasearray.put(purchase);
				purchasearray.put(purchase4);
			}
			
		
			
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History" + count);
			//assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  When suspending the account purchases are still active then re-activating one of the cancelled existing purchases and create one additional purchase/offer, it should return account status as 200 and account table gets updated.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6137623
	 */
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_004(){
		try {
			generatePurchaseRequest();
			
			JSONObject purchase1 = getPurchase();
			JSONObject purchase2 = getPurchase();
			JSONObject purchase3 = getPurchase();
			JSONObject purchase4 = getPurchase();
			
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2COL");
					//addQuantity("name", "2", purchase);
					
					purchase1.put("product_id", "TV2EXP");
					//addQuantity("name", "4", purchase);
					
					purchase2.put("product_id", "TV2JXN");
					//addQuantity("name", "6", purchase);
					
					purchasearray.put(purchase1);
					purchasearray.put(purchase2);
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				purchase2.put("product_id", "HBO");
				
				purchasearray.put(purchase1);
				purchasearray.put(purchase2);
			}
			
			
			
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			purchase1.remove("action");
			purchase1.put("action", "cancel");
			
			purchase2.remove("action");
			purchase2.put("action", "cancel");
			//reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + suspendJsonRes);
			assertNotNull(suspendJsonRes);
			
			//create
			generatePurchaseRequest();
			account.remove("status");
			account.put("status", "active");
			purchase.remove("action");
			purchase.put("action", "create");
			
			purchase1.remove("action");
			purchase1.put("action", "create");
			
			purchase2.remove("action");
			purchase2.put("action", "create");
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					
					purchase.put("product_id", "TV2COL");
					//addQuantity("name", "2", purchase);
					
					purchase1.put("product_id", "TV2EXP");
					//addQuantity("name", "4", purchase);
					
					purchase2.put("product_id", "TV2JXN");
					
					purchase3.remove("product_id");
					purchase3.put("product_id", "TV2LIFE");
					purchase4.put("product_id", "TV2MER");
					
					purchasearray.put(purchase3);
					purchasearray.put(purchase4);

			}else{
				purchase.put("product_id", "2_TV2STOR");
				purchase1.put("product_id", "MAX");
				purchase2.put("product_id", "HBO");
				
				purchase3.put("product_id", "Express");
				purchase4.put("product_id", "Essentials");
				
				purchasearray.put(purchase2);
				purchasearray.put(purchase1);
				purchasearray.put(purchase3);
				purchasearray.put(purchase4);
			}
			
		
			
			
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes, 0,externalid);
			int count =0;
			
			while (resultSet.next()) {
				log.info(resultSet.getString("BILLING_HISTORY_ID"));
			count++;
			}
			log.info("verifying Billing History" + count);
			//assertEquals(3, count);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description  With the suspended account status if user sends reactivation request without account status change, verify it returns 201 and no account is created.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference https://testrail.mobitv.corp/index.php?/tests/view/6163047
	 */
	@Ignore
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_005(){
		try {
			generatePurchaseRequest();
			
			Long purchaseId;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2STRM");
					addQuantity("name", "2", purchase);
					
					
					
				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			
			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			account.remove("status");
			account.put("status", "Suspend");
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + jsonRes);
			assertNotNull(jsonRes);
			
			//create
			
			
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.remove("product_id");
					
					purchase.put("product_id", "MAX");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.remove("product_id");
				
				purchase.put("product_id", "HBO");
			}
			
			reqobj.put("purchase", purchasearray);
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);
			
			
			log.info("" + conn.getResponseCode());
			
			log.info("verifying the response");
			assertEquals(201, conn.getResponseCode());
			
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description:For an existing user with suspended account, if user requests to update zip code verify response returns success and account should be in active state.
	 * 
	 * @test.verification Verifies the API returns status code 200.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/cases/view/3859307
	 */
	
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_006(){
		try {
			log.info("Starting Test case 06");
			generatePurchaseRequest();
			
			Long purchaseId = null;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}
			account.put("zip_code", "84007");

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			Log.info("Suspending the Account");
			
			account.remove("status");
			account.put("status", "Suspend");
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + suspendJsonRes);
			assertNotNull(suspendJsonRes);
			
			//create
			account.remove("status");
			account.put("status", "");
			
			
			reqobj.remove("purchase");

			
			account.remove("zip_code");
			account.put("zip_code", "12345");
			jsonRes = genericUtil.webResponse(externalid, reqobj);
			assertNotNull(suspendJsonRes);
		}else{
			log.info("Carrier.fips.code.config is not equal to default");
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
		finally {
			log.info("Ending Test case 06");
		}
	}
	
	
	/**
	 * @throws Exception
	 * 
	 * @test.type functional
	 * 
	 * @test.description: For an existing user with suspended account, if user requests to cancel purchase with account status as suspend, verify response returns failure.
	 * 
	 * @test.verification Verifies the API returns status code 400.
	 * 
	 * 
	 * @test.reference:https://testrail.mobitv.corp/index.php?/cases/view/3859308
	 */
	
	@Test
	public void SSC_Generic_billing_adapter_Specific_package_changes_007(){
		try {
			log.info("Starting Test case 07");
			generatePurchaseRequest();
			
			Long purchaseId = null;
			ResultSet resultSet = null;
			
			account.put("status", "active");
			if (configreader.getConfigValue(carrier+".fips.code.config").equals("default")) {
			if (configreader.getConfigValue(carrier + ".feature.offer.config").equals("quantity")) {
				try {
					
					log.info(configreader.getConfigValue(carrier + ".feature.offer.config"));
					
					purchase.put("product_id", "TV2JXN");

				} catch (Exception e) {
					log.error(e.getMessage());
				}
			}else{
				purchase.put("product_id", "2_TV2STOR");
			}

			JSONObject jsonRes = genericUtil.webResponse(externalid, reqobj);
			
			purchaseId = (Long) genericUtil.getDataFromJSONResponse(jsonRes, "purchase_id", 0);
			
			resultSet = commonTableValidators.getBillingHistoryTable(jsonRes,0,externalid);
			
			
			//suspend
			Log.info("Suspending the Account");
			
			account.remove("status");
			account.put("status", "suspend");
			
			
			reqobj.remove("purchase");
			
			JSONObject suspendJsonRes = genericUtil.webResponse(externalid, reqobj);
			
			log.debug("Create Purchases Response: " + suspendJsonRes);
			assertNotNull(suspendJsonRes);
			
			//create
			Log.info("Suspending the Account and cancel action");
			account.remove("status");
			account.put("status", "suspend");
			
			reqobj.put("purchase", purchasearray);
			
			purchase.remove("action");
			purchase.put("action", "cancel");
			
			HttpURLConnection conn = genericUtil.webStatusResponse(externalid, reqobj);

			log.info(""+conn.getResponseCode());
			log.info("Verifying the Response");
			assertEquals(400,conn.getResponseCode());
		}else{
			
			fail();
		}
	}
		catch (Exception e) {
			log.info(e.getMessage());
			fail();
		}
		finally {
			log.info("Ending Test case 07");
		}
	}
}


